package com.abcode.nativevideo;

import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.abcode.nativevideo.codec.ConcatenateSequentialTest;
import com.abcode.nativevideo.codec.ExtractDecodeEditEncodeMuxTest;
import com.abcode.nativevideo.utils.Constant;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends FragmentActivity implements VideoSelectorFragment.OnVideoSelectedListener, LoaderManager.LoaderCallbacks<Cursor>,
        View.OnClickListener {

    private static final int VIDEO_CURSOR_LOADER_ID = 10;

    private static final String[] PROJECTION = new String[] { MediaStore.Video.Media._ID, MediaStore.Video.Media.DATA, MediaStore.Video.Media.DATE_ADDED };

    private VideoSelectorFragment vsf;
    private FrameLayout selectorLayout;
    private Button selectVideoBtn, processVideoBtn;

    private CoordinatorLayout actRoot;

    private String videoPath;
    private ProgressBar pb;
    private RelativeLayout pbLayout;

    private VideoView vidView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vsf = new VideoSelectorFragment();

        selectorLayout = findViewById(R.id.chooser_fragment_layout);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.chooser_fragment_layout, vsf)
                .commit();

        selectVideoBtn = findViewById(R.id.select_vid_btn);
        processVideoBtn = findViewById(R.id.process_vid_btn);
        selectVideoBtn.setOnClickListener(this);
        processVideoBtn.setOnClickListener(this);

        pb = findViewById(R.id.encoding_progress_bar);
        pbLayout = findViewById(R.id.encoding_process_layout);
        vidView = findViewById(R.id.result_video_preview);

        actRoot = findViewById(R.id.main_act_root);

        getSupportLoaderManager().initLoader(VIDEO_CURSOR_LOADER_ID, null, this);
    }

    private void startProcessVideo() {

        new AsyncTask<Void, Void, String>() {
            String width;
            String height;
            String orient;
            String bitrate;
            boolean hasAudio;

            ArrayList<Uri> uriList;

            Throwable error;

            long start;
            long duration;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                start = System.currentTimeMillis();

                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                Log.i("chosen_video_path", videoPath+"");
                mmr.setDataSource(videoPath);
                width  = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
                height = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
                orient = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
                bitrate = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE);
                hasAudio = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_HAS_AUDIO) != null;
                Log.i("video_has_sound", hasAudio+"");
                pbLayout.setVisibility(View.VISIBLE);
                mmr.release();
            }

            @Override
            protected String doInBackground(Void... voids) {
                String resultPath = "";
                ExtractDecodeEditEncodeMuxTest cst = new ExtractDecodeEditEncodeMuxTest(MainActivity.this);
                try {

                    resultPath = cst.testExtractDecodeEditEncodeMuxAudioVideo(videoPath, Constant.VINTAGE, hasAudio, Integer.valueOf(width), Integer.valueOf(height), Integer.valueOf(orient), Integer.valueOf(bitrate));
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                return resultPath;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                duration = System.currentTimeMillis() - start;
                pbLayout.setVisibility(View.GONE);
                //Toast.makeText(MainActivity.this, "Encoding success", Toast.LENGTH_SHORT).show();
                vidView.setVideoPath(s);
                vidView.start();

                final Snackbar snackbar = Snackbar.make(actRoot, "Encoding finished in "+duration+" ms", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                snackbar.dismiss();
                            }
                        });
                snackbar.show();
            }
        }.execute();
    }

    @Override
    public void onVideoSelected(String videoPath) {
        Log.i("video_path", videoPath);
        this.videoPath = videoPath;
        selectorLayout.setVisibility(View.GONE);
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int i, @Nullable Bundle bundle) {
        return new CursorLoader(this, MediaStore.Video.Media.EXTERNAL_CONTENT_URI, PROJECTION, null, null, MediaStore.Video.Media.DATE_ADDED+" DESC" );
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {
        Log.i("cursor_count", cursor.getCount()+"");
        vsf.onCursorFinished(cursor);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }

    @Override
    public void onClick(View view) {
        if(view == selectVideoBtn) {
            selectorLayout.setVisibility(View.VISIBLE);
        } else if(view == processVideoBtn) {
            startProcessVideo();
        }
    }
}
