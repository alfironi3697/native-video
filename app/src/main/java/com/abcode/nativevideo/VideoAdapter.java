package com.abcode.nativevideo;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.abcode.nativevideo.utils.GlideApp;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;

public class VideoAdapter extends CursorRecyclerAdapter {

    private VideoSelectorFragment.OnVideoSelectedListener listener;
    Context c;

    public VideoAdapter(Context c, Cursor cu, VideoSelectorFragment.OnVideoSelectedListener l) {
        super(c, cu);
        this.c = c;
        this.listener = l;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        //return super.onCreateViewHolder(viewGroup, type);
        View inf = LayoutInflater.from(c).inflate(R.layout.video_adapter_viewholder, null, false);
        return new ViewHolder(inf);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, Cursor cursor) {
        if(viewHolder instanceof ViewHolder) {
            ViewHolder vh = (ViewHolder)viewHolder;
            int index = cursor.getColumnIndex(MediaStore.Video.Media.DATA);
            int idIndex = cursor.getColumnIndex(MediaStore.Video.Media.DATA);
            String data = cursor.getString(index);
            int id = cursor.getInt(idIndex);
            //Bitmap b = ThumbnailUtils.createVideoThumbnail(data, MediaStore.Video.Thumbnails.MICRO_KIND);
            vh.filepath = data;
            //vh.iv.setImageBitmap(b);
            Glide.with(context)
                    //.asBitmap()
                    .load(vh.filepath)
                    //.thumbnail(0.1f)
                    .apply(new RequestOptions().centerCrop().diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                    .into(vh.iv);

            //MediaStore.Video.Thumbnails.getThumbnail(context.getContentResolver(), id, MediaStore.Video.Thumbnails.);
            vh.listener = this.listener;
        }

    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }


    private class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView iv;
        RelativeLayout f;
        String filepath;
        VideoSelectorFragment.OnVideoSelectedListener listener;

        ViewHolder(View v) {
            super(v);
            f = v.findViewById(R.id.vh_root);
            iv = v.findViewById(R.id.image);
            iv.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(view == iv) {
                listener.onVideoSelected(filepath);
            }
        }
    }
}
