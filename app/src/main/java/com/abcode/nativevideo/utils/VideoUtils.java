package com.abcode.nativevideo.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.abcode.nativevideo.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


/**
 * Created by alfironi on 6/26/2015.
 */
public class VideoUtils {
    private static final String TAG = "Utils";
    private static RandomAccessFile randomAccessFile;
    private static int TARGET_SIZE = 720;

    public static Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    public static File getOutputMediaFile(int type) {
        File mediaStorageDir = Environment
                .getExternalStoragePublicDirectory(Constant.DIRECTORY_PICMIX_VIDEO);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "failed to create directory");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == Constant.MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else if (type == Constant.MEDIA_TYPE_FRAME) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "FRAME_" + timeStamp + ".png");
        } else if (type == Constant.MEDIA_TYPE_COVER) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "FRAME_" + timeStamp + ".JPEG");
        } else {
            return null;
        }

        return mediaFile;
    }

    public static String getPrelimFragmentShader(Context ctx, int prelimShaderSelection) {
        int id = R.raw.shader_normal;
        switch (prelimShaderSelection) {
            case Constant.NORMAL:
                id = R.raw.shader_normal;
                break;
            case Constant.VINTAGE:
                id = R.raw.shader_1977;
                break;
            case Constant.AMARO:
                id = R.raw.shader_amaro;
                break;
            case Constant.BRANNAN:
                id = R.raw.shader_brannan;
                break;
            case Constant.EARLYBIRD:
                id = R.raw.shader_earlybird;
                break;
            case Constant.HEFE:
                id = R.raw.shader_hefe;
                break;
            case Constant.HUDSON:
                id = R.raw.shader_hudson;
                break;
            case Constant.INKWELL:
                id = R.raw.shader_inkwell;
                break;
            case Constant.LOMO:
                id = R.raw.shader_lomo;
                break;
            case Constant.LORDKELVIN:
                id = R.raw.shader_lordkelvin;
                break;
            case Constant.NASHVILLE:
                id = R.raw.shader_nashville;
                break;
            case Constant.RISE:
                id = R.raw.shader_rise;
                break;
            case Constant.SIERRA:
                id = R.raw.shader_sierra;
                break;
            case Constant.SUTRO:
                id = R.raw.shader_sutro;
                break;
            case Constant.TOASTER:
                id = R.raw.shader_toaster;
                break;
            case Constant.VALENCIA:
                id = R.raw.shader_valencia;
                break;
            case Constant.WALDEN:
                id = R.raw.shader_walden;
                break;
            case Constant.XPROLL:
                id = R.raw.shader_xproll;
                break;
            default:
                id = R.raw.shader_normal;
                break;
        }
        return RawResourceReader.readTextFileFromRawResource(ctx, id);
    }

    public static String getCameraRendererViableFragmentShader(Context context, int position)
    {
        String rawFragmentShader = getPrelimFragmentShader(context, position);
        //String cleanFragmentShader = rawFragmentShader.replaceAll("u_Texture", "inputImageTexture").replaceAll("v_TexCoordinate", "textureCoordinate").replace("samplerExternalOES", "sampler2D").replace("#extension GL_OES_EGL_image_external : require\n", "");
        String cleanFragmentShader = rawFragmentShader.replaceAll("u_Texture", "sTexture");
        return cleanFragmentShader;
    }

    public static int[] getFilterAssets(int selection)
    {
        Log.i("prelim_filter_assets", selection+"");
        int[] resIds;
        switch(selection)
        {
            case Constant.VINTAGE:
                resIds = new int[]{R.drawable.nmap};
                break;
            case Constant.AMARO:
                resIds = new int[]{R.drawable.blackboard, R.drawable.overlay_map, R.drawable.amaro_map};
                break;
            case Constant.BRANNAN:
                resIds = new int[]{R.drawable.brannan_process, R.drawable.brannan_blowout, R.drawable.brannan_contrast, R.drawable.brannan_luma, R.drawable.brannan_screen};
                break;
            case Constant.EARLYBIRD:
                resIds = new int[]{R.drawable.earlybird_curves, R.drawable.earlybird_overlay_map, R.drawable.vignette_map, R.drawable.earlybird_blowout, R.drawable.earlybird_map};
                break;
            case Constant.HEFE:
                resIds = new int[]{R.drawable.edge_burn, R.drawable.hefe_map, R.drawable.hefe_gradient_map, R.drawable.hefe_soft_light, R.drawable.hefe_metal};
                break;
            case Constant.HUDSON:
                resIds = new int[]{R.drawable.hudson_background, R.drawable.overlay_map, R.drawable.hudson_map};
                break;
            case Constant.INKWELL:
                resIds = new int[]{R.drawable.inkwell_map};
                break;
            case Constant.LOMO:
                resIds = new int[]{R.drawable.lomo_map, R.drawable.vignette_map};
                break;
            case Constant.LORDKELVIN:
                resIds = new int[]{R.drawable.kelvin_map};
                break;
            case Constant.NASHVILLE:
                resIds = new int[]{R.drawable.nashville_map};
                break;
            case Constant.RISE:
                resIds = new int[]{R.drawable.blackboard, R.drawable.overlay_map, R.drawable.rise_map};
                break;
            case Constant.SIERRA:
                resIds = new int[]{R.drawable.sierra_vignette, R.drawable.overlay_map, R.drawable.sierra_map};
                break;
            case Constant.SUTRO:
                resIds = new int[]{R.drawable.vignette_map, R.drawable.sutro_metal, R.drawable.soft_light, R.drawable.sutro_edge_burn, R.drawable.sutro_curves};
                break;
            case Constant.TOASTER:
                resIds = new int[]{R.drawable.toaster_metal, R.drawable.toaster_soft_light, R.drawable.toaster_curves, R.drawable.toaster_overlay_map_warm, R.drawable.toaster_color_shift};
                break;
            case Constant.VALENCIA:
                resIds = new int[]{R.drawable.valencia_map, R.drawable.valencia_gradient_map};
                break;
            case Constant.WALDEN:
                resIds = new int[]{R.drawable.walden_map, R.drawable.vignette_map};
                break;
            case Constant.XPROLL:
                resIds = new int[]{R.drawable.xpro_map, R.drawable.vignette_map};
                break;
            default:
                resIds = new int[]{};
                break;
        }
        return resIds;
    }

    public static void convertBitmapToJpg(Bitmap bmp, File file, boolean isFit)
            throws IOException {
        Bitmap bitmap = null;
        if (isFit) {
            bitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight());
        } else {
            bitmap = Bitmap.createScaledBitmap(bmp, TARGET_SIZE, TARGET_SIZE, false);
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.JPEG, 90, bos);
        byte[] bitmapdata = bos.toByteArray();


        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bitmapdata);

        bos.close();
        fos.close();
    }

    public static Bitmap getVideoFrame(String uri, Integer time, boolean isFromRecord, float density, int mScreenHeight, boolean isFit) {
        Bitmap bmp = Bitmap.createBitmap(TARGET_SIZE, TARGET_SIZE, Bitmap.Config.ARGB_8888);
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(uri);
            long oneMicroSecond = TimeUnit.SECONDS.toMicros(time);
            Bitmap bitmap = retriever.getFrameAtTime(oneMicroSecond,
                    MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
            retriever.release();
            if (isFit) {
                if (bitmap.getWidth() > bitmap.getHeight()) {
                    bmp = Bitmap.createScaledBitmap(bitmap, TARGET_SIZE, bitmap.getHeight() * TARGET_SIZE / bitmap.getWidth(), false);
                } else {
                    bmp = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() * TARGET_SIZE / bitmap.getHeight(), TARGET_SIZE, false);
                }
            } else {
                bmp = cropBitmap(bitmap, isFromRecord, density, mScreenHeight, isFit);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bmp;
    }

    public static Bitmap getVideoFrameWithOffsetFinal(String uri, float time, boolean isFromRecord, float density, int mScreenHeight, boolean isFit, int xOffsetFromTopLeft, int yOffsetFromTopLeft) {
        Bitmap bmp = Bitmap.createBitmap(TARGET_SIZE, TARGET_SIZE, Bitmap.Config.ARGB_8888);
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(uri);
            //long oneMicroSecond = TimeUnit.SECONDS.toMicros(time);
            long oneMicroSecond = (long)(time * 1000000);
            Bitmap bitmap = retriever.getFrameAtTime(oneMicroSecond,
                    MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
            retriever.release();
            if (isFit) {
                if (bitmap.getWidth() > bitmap.getHeight()) {
                    bmp = Bitmap.createScaledBitmap(bitmap, TARGET_SIZE, bitmap.getHeight() * TARGET_SIZE / bitmap.getWidth(), false);
                } else {
                    bmp = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() * TARGET_SIZE / bitmap.getHeight(), TARGET_SIZE, false);
                    //bmp = getVideoFrameWithOffset(uri, time, isFromRecord, density, mScreenHeight, isFit);
                }
            } else {
                //bmp = cropBitmap(bitmap, isFromRecord, density, mScreenHeight, isFit);
                bmp = cropBitmapWithOffset(bitmap, isFromRecord, density, mScreenHeight, isFit, xOffsetFromTopLeft, yOffsetFromTopLeft);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bmp;
    }

    public static Bitmap getVideoFrameWithOffsetFinal(Context ctx, String uri, float time, boolean isFromRecord, float density, int mScreenHeight, boolean isFit, int xOffsetFromTopLeft, int yOffsetFromTopLeft) {
        Bitmap bmp = Bitmap.createBitmap(TARGET_SIZE, TARGET_SIZE, Bitmap.Config.ARGB_8888);
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(ctx, Uri.parse(uri));
            //long oneMicroSecond = TimeUnit.SECONDS.toMicros(time);
            long oneMicroSecond = (long)(time * 1000000);
            Bitmap bitmap = retriever.getFrameAtTime(oneMicroSecond,
                    MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
            retriever.release();
            if (isFit) {
                if (bitmap.getWidth() > bitmap.getHeight()) {
                    bmp = Bitmap.createScaledBitmap(bitmap, TARGET_SIZE, bitmap.getHeight() * TARGET_SIZE / bitmap.getWidth(), false);
                } else {
                    bmp = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() * TARGET_SIZE / bitmap.getHeight(), TARGET_SIZE, false);
                    //bmp = getVideoFrameWithOffset(uri, time, isFromRecord, density, mScreenHeight, isFit);
                }
            } else {
                //bmp = cropBitmap(bitmap, isFromRecord, density, mScreenHeight, isFit);
                bmp = cropBitmapWithOffset(bitmap, isFromRecord, density, mScreenHeight, isFit, xOffsetFromTopLeft, yOffsetFromTopLeft);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bmp;
    }

    public static Bitmap getVideoFrameWithOffset(final String uri, float time, boolean isFromRecord, float density, int mScreenHeight, boolean isFit, int xOffsetFromTopLeft, int yOffsetFromTopLeft) {
        Bitmap bmp = Bitmap.createBitmap(TARGET_SIZE, TARGET_SIZE, Bitmap.Config.ARGB_8888);
        try {
            final MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(uri);
            //long oneMicroSecond = TimeUnit.SECONDS.toMicros(time);
            long oneMicroSecond = (long)(time * 1000000);
            Bitmap bitmap = retriever.getFrameAtTime(oneMicroSecond,
                    MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
            retriever.release();
            if (isFit) {
                //bmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight());
                float wScale = (float)bitmap.getWidth() / 320;
                float hScale = (float)bitmap.getHeight() / 320;
                int scaledWidth, scaledHeight;
                Rect drawRect;
                if(wScale > hScale)
                {
                    scaledWidth = (int)(bitmap.getWidth() / wScale);
                    scaledHeight = (int)(bitmap.getHeight() / wScale);
                    drawRect = new Rect(0, (scaledWidth - scaledHeight)/2, 320, (scaledWidth + scaledHeight)/2);
                }
                else
                {
                    scaledWidth = (int)(bitmap.getWidth() / hScale);
                    scaledHeight = (int)(bitmap.getHeight() / hScale);
                    drawRect = new Rect((scaledHeight - scaledWidth)/2, 0, (scaledHeight + scaledWidth)/2, 320);
                }
                //bmp = Bitmap.createScaledBitmap(bitmap, scaledWidth, scaledHeight, true);
                bmp = Bitmap.createBitmap(320, 320, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bmp);
                canvas.drawColor(Color.BLACK);
                canvas.drawBitmap(bitmap, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), drawRect, null);
            } else {
                bmp = cropBitmapWithOffset(bitmap, isFromRecord, density, mScreenHeight, isFit, xOffsetFromTopLeft, yOffsetFromTopLeft
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bmp;
    }

    public static Bitmap cropBitmap(Bitmap srcBmp, boolean isFromRecord, float density, int mScreenHeight, boolean isFit) {
        Bitmap dstBmp = Bitmap.createBitmap(TARGET_SIZE, TARGET_SIZE, Bitmap.Config.ARGB_8888);
        Bitmap fitBmp;
        int pivotY = (int) (50 * density * srcBmp.getHeight() / mScreenHeight);
        try {
            if (srcBmp != null) {
                if (isFromRecord) {
                    dstBmp = Bitmap.createBitmap(srcBmp, 0, pivotY, srcBmp.getWidth(),
                            srcBmp.getWidth());
                } else {
                    if (srcBmp.getWidth() >= srcBmp.getHeight()) {
                        if (isFit) {
                            float ratio = TARGET_SIZE / srcBmp.getWidth();
                            int targetWidth = (int) (srcBmp.getWidth() * ratio);
                            int targetHeight = (int) (srcBmp.getHeight() * ratio);
                            fitBmp = Bitmap.createScaledBitmap(srcBmp, targetWidth, targetHeight, true);
                            Canvas c = new Canvas(dstBmp);
                            c.drawColor(Color.BLACK);
                            c.drawBitmap(fitBmp, 0, (c.getHeight() - fitBmp.getHeight()) / 2, null);
                        } else {
                            dstBmp = Bitmap.createBitmap(srcBmp,
                                    srcBmp.getWidth() / 2 - srcBmp.getHeight() / 2, 0,
                                    srcBmp.getHeight(), srcBmp.getHeight());
                        }
                    } else {
                        if (isFit) {
                            float ratio = TARGET_SIZE / srcBmp.getHeight();
                            int targetWidth = (int) (srcBmp.getWidth() * ratio);
                            int targetHeight = (int) (srcBmp.getHeight() * ratio);
                            fitBmp = Bitmap.createScaledBitmap(srcBmp, targetWidth, targetHeight, true);
                            Canvas c = new Canvas(dstBmp);
                            c.drawColor(Color.BLACK);
                            c.drawBitmap(fitBmp, (c.getWidth() - fitBmp.getWidth()) / 2, 0, null);
                        } else {
                            dstBmp = Bitmap.createBitmap(srcBmp, 0, srcBmp.getHeight() / 2 - srcBmp.getWidth() / 2,
                                    srcBmp.getWidth(), srcBmp.getWidth());
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dstBmp;
    }

    public static Bitmap cropBitmapWithOffset(Bitmap srcBmp, boolean isFromRecord, float density, int mScreenHeight, boolean isFit, int xOffsetFromTopLeft, int yOffsetFromTopLeft) {
        Bitmap dstBmp = Bitmap.createBitmap(TARGET_SIZE, TARGET_SIZE, Bitmap.Config.ARGB_8888);
        Bitmap fitBmp;
        //int pivotY = (int) (50 * density * srcBmp.getHeight() / mScreenHeight);
        //int pivotY = (int) (50 * density * PicMixApp.sDisplayWidth / mScreenHeight);
        int pivotY = 150;
        //int pivotY = (int) (50 * density * (srcBmp.getHeight()+TARGET_SIZE) / mScreenHeight);
        Log.i("y_offset_bitmap", pivotY+" "+density+" "+srcBmp.getHeight()+" "+mScreenHeight);
        try {
            if (srcBmp != null) {
                if (isFromRecord) {
                    dstBmp = Bitmap.createBitmap(srcBmp, 0, pivotY, srcBmp.getWidth(), srcBmp.getWidth());
                } else {
                    if (srcBmp.getWidth() >= srcBmp.getHeight()) {
                        if (isFit) {
                            float ratio = (float)TARGET_SIZE / srcBmp.getWidth();
                            int targetWidth = (int) (srcBmp.getWidth() * ratio);
                            int targetHeight = (int) (srcBmp.getHeight() * ratio);
                            fitBmp = Bitmap.createScaledBitmap(srcBmp, targetWidth, targetHeight, true);
                            Canvas c = new Canvas(dstBmp);
                            c.drawColor(Color.BLACK);
                            c.drawBitmap(fitBmp, 0, (c.getHeight() - fitBmp.getHeight()) / 2, null);
                        } else {
                            Log.i("crop_dims", srcBmp.getWidth()+" "+srcBmp.getHeight()+" "+xOffsetFromTopLeft+" "+yOffsetFromTopLeft);
                            int horizontalOffset = xOffsetFromTopLeft;
                            if(horizontalOffset > (srcBmp.getWidth() - srcBmp.getHeight())){horizontalOffset = (srcBmp.getWidth() - srcBmp.getHeight());}
                            else if(horizontalOffset < 0){horizontalOffset = (srcBmp.getWidth() - srcBmp.getHeight())/2;}
                            else{}
                            //dstBmp = Bitmap.createBitmap(srcBmp, srcBmp.getWidth() / 2 - srcBmp.getHeight() / 2, 0, srcBmp.getHeight(), srcBmp.getHeight());
                            dstBmp = Bitmap.createBitmap(srcBmp, horizontalOffset, 0, srcBmp.getHeight(), srcBmp.getHeight());
                        }
                    } else {
                        if (isFit) {
                            float ratio = (float)TARGET_SIZE / srcBmp.getHeight();
                            int targetWidth = (int) (srcBmp.getWidth() * ratio);
                            int targetHeight = (int) (srcBmp.getHeight() * ratio);
                            fitBmp = Bitmap.createScaledBitmap(srcBmp, targetWidth, targetHeight, true);
                            Canvas c = new Canvas(dstBmp);
                            c.drawColor(Color.BLACK);
                            c.drawBitmap(fitBmp, (c.getWidth() - fitBmp.getWidth()) / 2, 0, null);
                        } else {
                            Log.i("crop_dims", srcBmp.getWidth()+" "+srcBmp.getHeight()+" "+xOffsetFromTopLeft+" "+yOffsetFromTopLeft);
                            int verticalOffset = yOffsetFromTopLeft;
                            if(verticalOffset > (srcBmp.getHeight() - srcBmp.getWidth())){verticalOffset = (srcBmp.getHeight() - srcBmp.getWidth());}
                            else if(verticalOffset < 0){verticalOffset = (srcBmp.getHeight() - srcBmp.getWidth())/2;}
                            else{}
                            //dstBmp = Bitmap.createBitmap(srcBmp, 0, srcBmp.getHeight() / 2 - srcBmp.getWidth() / 2, srcBmp.getWidth(), srcBmp.getWidth());
                            dstBmp = Bitmap.createBitmap(srcBmp, 0, verticalOffset, srcBmp.getWidth(), srcBmp.getWidth());
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dstBmp;
    }

    /*
    public static String rotateVideo(String path, int orientation) {
        try {
            Matrix matrix = null;
            switch (orientation) {
                case 0:
                    matrix = Matrix.ROTATE_0;
                    break;
                case 90:
                    matrix = Matrix.ROTATE_90;
                    break;
                case 180:
                    matrix = Matrix.ROTATE_180;
                    break;
                case 270:
                    matrix = Matrix.ROTATE_270;
                    break;
            }
            Movie inMovie = MovieCreator.build(path);
            inMovie.setMatrix(matrix);

            Container out = new DefaultMp4Builder().build(inMovie);
            String pathVideo = getOutputMediaFile(Constant.MEDIA_TYPE_VIDEO).toString();
            FileOutputStream fos = new FileOutputStream(pathVideo);
            FileChannel fc = fos.getChannel();
            out.writeContainer(fc);
            fc.close();
            fos.close();
            return pathVideo;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    */
}
