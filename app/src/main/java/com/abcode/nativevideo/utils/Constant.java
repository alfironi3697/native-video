package com.abcode.nativevideo.utils;

/**
 * Created by alfironi on 6/26/2015.
 */
public class Constant {
    public static final String PREFERENCES_NAME = "PicMix_Video_Preferences";
    public static final String PREFERENCES_ISFIRSTINSTALL = "Firt_Install";
    public static final String DEFAULT_TIME = "0:00";
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int MEDIA_TYPE_FRAME = 1;
    public static final int MEDIA_TYPE_COVER = 3;
    public static final int SELECT_VIDEO_GALERY = 1;
    public static final int SELECT_IMAGE_GALERY = 2;
    public static final int GALLERY_KITKAT_INTENT_CALLED = 2;
    public static final String DEV_TYPE_VALUE = "2";
    public static final int LOOP_PLAY_COUNT = 2;

    // requirement for directory
    public static final String DIRECTORY_PICMIX_VIDEO = "PicMix Video";
    public static final String DIRECTORY_IMAGE = "PicMix Image";

    // intent bundle parameter
    public static final String PARAM_URI_MERGE_VIDEO = "param_uri_merge_video";
    public static final String PARAM_VIDEO_DURATION = "param_video_duration";
    public static final String PARAM_POSITION = "param_position";
    public static final String PARAM_LIST_URI_VIDEO = "param_list_uri_video";
    public static final String PARAM_LIST_BITMAP = "param_list_bitmap";
    public static final String PARAM_COVER = "param_cover";
    public static final String PARAM_ISFIT = "param_isfit";
    public static final String PARAM_VIDEO = "param_video";
    public static final String PARAM_URI_TRIM_VIDEO = "param_uri_trim_video";
    public static final String PARAM_HINT_VIDEO = "param_hint_video";
    public static final String PARAM_SOUND_VIDEO = "param_sound_video";
    public static final String PARAM_FRAME_VIDEO = "param_frame_video";
    public static final String PARAM_TITLE_POST = "param_title_post";
    public static final String PARAM_GROUP_POST_REQUEST = "param_group_post_request";

    // for the changeable colorize filter
    public static final String FILTER_PARAM_VALUES = "filter_param_values";

    // requirement for ACRA
    public static final String EMPTY = "";
    public static final String ACRA_URI = "http://23.21.198.78/stacktrace.php";
    public static final String SERVER_URL = "http://23.21.198.78/stacktrace.php";

    // special effects fragment_shader code
    static public final int NORMAL = 100;
    static public final int VINTAGE = 101;
    static public final int AMARO = 102;
    static public final int BRANNAN = 103;
    static public final int EARLYBIRD = 104;
    static public final int HEFE = 105;
    static public final int HUDSON = 106;
    static public final int INKWELL = 107;
    static public final int LOMO = 108;
    static public final int LORDKELVIN = 109;
    static public final int NASHVILLE = 110;
    static public final int RISE = 111;
    static public final int SIERRA = 112;
    static public final int SUTRO = 113;
    static public final int TOASTER = 114;
    static public final int VALENCIA = 115;
    static public final int WALDEN = 116;
    static public final int XPROLL = 117;

    // requirement for parameter upload to API
    static public final String COVER = "cover";
    static public final String SESSION_ID = "session_id";
    static public final boolean IS_REQUIRED_COVER = true;
    static public final boolean IS_REQUIRED_SESSION_ID = true;
    static public final String STATUS = "status";
    static public final String _ID = "_id";
    static public final String COVER_ID = "cover_id";
    static public final String THUMBNAIL = "thumbnail";
    static public final String TITLE = "title";
    static public final String VIDEO = "video";
    static public final String VENUE = "venue";
    static public final String DEV_TYPE = "devType";
    static public final String IS_PUBLIC = "public";

    // API URL
    static public final String BASE_API_URL = "http://transcode.picmix.it/picmix/video/";
    static public final String UPLOAD_COVER = "/upload_cover.php";
    static public final String UPLOAD_VIDEO = "/upload_video.php";

    // status response from API description status
    //static public final String STATUS_SERVER_ERROR = "001";
    //static public final String STATUS_INCOMPLETE_PARAMETER = "002";
    //static public final String STATUS_INVALID_SESSION = "006";
    static public final String STATUS_ERROR_SAVING_COVER_FILE = "010";
    static public final String STATUS_SUCCESS = "000";
    static public final String STATUS_COVER_ID_NOT_FOUND = "003";

    // array filter
    /*
    public static final int[] mColorFilter = {R.color.normal, R.color.maroon,
            R.color.darkor, R.color.green, R.color.mediblue, R.color.darkvio,
            R.color.rebecca, R.color.fuchsia, R.color.lightslate,
            R.color.crimson, R.color.orchid, R.color.purple, R.color.steelblue,
            R.color.azure, R.color.chartreuse, R.color.gold, R.color.darkslate,
            R.color.beige, R.color.lawngreen, R.color.violetred, R.color.snow};
    */

//    public static final int[] FRAMES = {R.drawable.frames_teater, R.drawable.frames_mecrowave,
//            R.drawable.frames_conference, R.drawable.frames_daily_rophet,
//            R.drawable.frames_hatihati,
//            R.drawable.frames_valentine, R.drawable.frames_tv};
    public static final int[] FRAMES = {};

    // To transfer coordinates from edit activity to share activity
    public static final String OPENGL_VIEWPORT_X = "gl_x";
    public static final String OPENGL_VIEWPORT_Y = "gl_y";
    public static final String OPENGL_VIEWPORT_OFFSET_ARRAY = "gl_x_array";
}
