package com.abcode.nativevideo.utils;

import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import java.nio.IntBuffer;

/**
 * Created by Ivan on 11/4/2016.
 */

public class GLTextureHelper
{
    public static int loadTexture(Bitmap texture)
    {
        IntBuffer textureIntBuf = IntBuffer.allocate(1);
        GLES20.glGenTextures(1, textureIntBuf);
        int textureHandle = textureIntBuf.get();

        if (textureHandle != 0) {
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle);

            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, texture, 0);
        }

        //if (textures[2] == 0) {
        if (textureHandle == 0) {
            throw new RuntimeException("Error loading texture.");
        }

        //return textures[2];
        return textureHandle;
    }
}
