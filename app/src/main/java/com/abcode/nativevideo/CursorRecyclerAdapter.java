package com.abcode.nativevideo;

import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinsen on 8/11/15.
 * edited by ati on 8/26/15.
 */
public abstract class CursorRecyclerAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    List<View> headers = new ArrayList<>();
    List<View> footers = new ArrayList<>();

    public static final int TYPE_HEADER = 111;
    public static final int TYPE_FOOTER = 222;
    public static final int TYPE_ITEM = 333;

    protected Context context;
    private Cursor mCursor;
    private boolean mDataValid;
    private int mRowIdColumn;
    private int mHeaderSize;

    //private DataSetObserver mDataSetObserver;

    public CursorRecyclerAdapter(Context context, Cursor cursor) {
        this.context = context;
        mCursor = cursor;
        mDataValid = cursor != null;
        mRowIdColumn = mDataValid ? mCursor.getColumnIndex("_id") : -1;
    }

    public Cursor getCursor() {
        return mCursor;
    }

    //add a header to the adapter
    public void addHeader(View header) {
        if (!headers.contains(header)) {
            headers.add(header);
            notifyItemInserted(headers.size() - 1);
            mHeaderSize = headers.size();
        }
    }

    //remove a header from the adapter
    public void removeHeader(View header) {
        if (headers.contains(header)) {
            //animate
            notifyItemRemoved(headers.indexOf(header));
            headers.remove(header);
            mHeaderSize = headers.size();
        }
    }

    public boolean containsFooter(View footer) {
        return footers.contains(footer);
    }

    //add a footer to the adapter
    public void addFooter(View footer) {
        if (!footers.contains(footer)) {
            footers.add(footer);
            notifyItemInserted(headers.size() + getCursorItemCount() + footers.size() - 1);
        }
    }

    //remove a footer from the adapter
    public boolean removeFooter(View footer) {
        if (footers.contains(footer)) {
            //animate
            Log.i("CursorRecyclerAdapter", "headerSize:" + headers.size() + ", itemCount:" + getCursorItemCount() + ", footersIndex:" + footers.indexOf(footer));
            notifyItemRemoved(headers.size() + getCursorItemCount() + footers.indexOf(footer));
            footers.remove(footer);
            return true;
        } else {
            notifyDataSetChanged();
        }
        return false;
    }

    private int getCursorItemCount() {
        if (mDataValid && mCursor != null) {
            try {
                return mCursor.getCount();
            } catch (Exception e) {
                return 0;
            }
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        return headers.size() + getCursorItemCount() + footers.size();
    }

    @Override
    public long getItemId(int position) {
        if (mDataValid && mCursor != null && mCursor.moveToPosition(position)) {
            return mCursor.getLong(mRowIdColumn);
        }
        return 0;
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(true);
    }

    public abstract void onBindViewHolder(VH viewHolder, Cursor cursor);

    @Override
    public void onBindViewHolder(VH viewHolder, int position) {
        if (position < headers.size()) {
            prepareHeaderFooter((HeaderFooterViewHolder) viewHolder, headers.get(position));
        } else if (position >= headers.size() + getCursorItemCount()) {
            prepareHeaderFooter((HeaderFooterViewHolder) viewHolder, footers.get(position - headers.size() - getCursorItemCount()));
        } else {
            if (!mDataValid) {
                throw new IllegalStateException("this should only be called when the cursor is valid");
            }

            if (mHeaderSize < 1) {
                if (!mCursor.moveToPosition(position)) {
                    throw new IllegalStateException("couldn't move cursor to position " + (position) + " cursor size is " + mCursor.getCount());
                }
            } else {
                if (!mCursor.moveToPosition(position - mHeaderSize)) {
                    throw new IllegalStateException("couldn't move cursor to position " + (position - mHeaderSize) + " cursor size is " + mCursor.getCount());
                }
            }
        }
        onBindViewHolder(viewHolder, mCursor);
    }

    @Override
    public VH onCreateViewHolder(ViewGroup viewGroup, int type) {
        FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
        frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return (VH) new HeaderFooterViewHolder(frameLayout);
    }

    private void prepareHeaderFooter(HeaderFooterViewHolder vh, View view) {
        vh.base.removeAllViews();
        Log.i(CursorRecyclerAdapter.class.getName(), "parent:" + view.getParent());
        if (view.getParent() instanceof ViewGroup) {
            ((ViewGroup) view.getParent()).removeAllViews();
        }
        vh.base.addView(view);
    }

    @Override
    public int getItemViewType(int position) {
        //check what type our position is, based on the assumption that the order is headers > items > footers
        if (position < headers.size()) {
            return TYPE_HEADER;
        } else if (position >= headers.size() + getCursorItemCount()) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private void changeCursor(Cursor cursor) {
        /*
        Cursor old = swapCursor(cursor);
        if (old != null) {
            old.close();
        }*/
    }

    public void swapCursor(Cursor newCursor) {
        if (newCursor == mCursor) {
            return;
        }

        final Cursor oldCursor = mCursor;
        //if (oldCursor != null && mDataSetObserver != null) {
        //    oldCursor.unregisterDataSetObserver(mDataSetObserver);
        //}
        mCursor = newCursor;
        if (mCursor != null) {
            //if (mDataSetObserver != null) {
            //    mCursor.registerDataSetObserver(mDataSetObserver);
            //}
            mRowIdColumn = newCursor.getColumnIndexOrThrow("_id");
            mDataValid = true;
            notifyDataSetChanged();
        } else {
            mRowIdColumn = -1;
            mDataValid = false;
            notifyDataSetChanged();
        }

        if (oldCursor != null) {
            oldCursor.close();
        }
        //return oldCursor;
    }

    public static class HeaderFooterViewHolder extends RecyclerView.ViewHolder {
        FrameLayout base;

        public HeaderFooterViewHolder(View itemView) {
            super(itemView);
            this.base = (FrameLayout) itemView;
        }
    }

    private class NotifyingDataSetObserver extends DataSetObserver {
        @Override
        public void onChanged() {
            super.onChanged();
            mDataValid = true;
            notifyDataSetChanged();
        }

        @Override
        public void onInvalidated() {
            super.onInvalidated();
            mDataValid = false;
            notifyDataSetChanged();
        }
    }
}
