package com.abcode.nativevideo.codec;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.util.Log;
import android.view.Surface;

import com.abcode.nativevideo.utils.Constant;
import com.abcode.nativevideo.utils.VideoUtils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by alfironi on 6/26/2015.
 */
@TargetApi(18)
public class ExtractDecodeEditEncodeMuxTest {

    private static final String TAG = ExtractDecodeEditEncodeMuxTest.class
            .getSimpleName();

    private int rate_gifVideo = 0;
    private static final boolean VERBOSE = true;
    private static final int TIMEOUT_USEC = 10000;
    private static final String OUTPUT_VIDEO_MIME_TYPE = "video/avc";
    private static int OUTPUT_VIDEO_BIT_RATE = 3200000;
    private static int OUTPUT_VIDEO_FRAME_RATE = 30;
    private static final int OUTPUT_VIDEO_IFRAME_INTERVAL = 10;
    private static final int OUTPUT_VIDEO_COLOR_FORMAT = MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface;

    // parameters for the audio encoder
    private static final String OUTPUT_AUDIO_MIME_TYPE = "audio/mp4a-latm";
    private static int OUTPUT_AUDIO_CHANNEL_COUNT = 2;
    private static int OUTPUT_AUDIO_BIT_RATE = 192 * 1024;
    private static final int OUTPUT_AUDIO_AAC_PROFILE = MediaCodecInfo.CodecProfileLevel.AACObjectLC;

    private Context mContext;
    private static String FRAGMENT_SHADER = "";
    private boolean mCopyVideo;
    private boolean mCopyAudio;
    private int mWidth = -1;
    private int mHeight = -1;
    private String mUri;
    private String mOutputFile;
    private boolean isHaveSound;
    private boolean isFromRecord;
    private int mScreenHeight, videoWidht, videoHeight, orientation;
    private int sampleRate = 44100;

    private int originalVideoFrameRate;
    private int timestampConstant;
    private int themePos;

    private float viewportX, viewportY;

    public ExtractDecodeEditEncodeMuxTest(Context context) {
        mContext = context;
    }

    public String testExtractDecodeEditEncodeMuxAudioVideo(String uri, int position,
                                                           boolean isHaveSound, int videoWidth, int videoHeight,
                                                           int orientation, int videoBitRate)
            throws Throwable {
        OUTPUT_VIDEO_BIT_RATE = 3200000;
        this.themePos = position;
        FRAGMENT_SHADER = VideoUtils.getPrelimFragmentShader(mContext, position);
        FRAGMENT_SHADER = FRAGMENT_SHADER.replace("uniform float uParamValue1", "const float uParamValue1 = 1.0");
        this.orientation = orientation;
        this.isHaveSound = isHaveSound;
        this.videoWidht = videoWidth;
        this.videoHeight = videoHeight;
        if(this.orientation % 180 == 90) {
            this.videoWidht = videoHeight;
            this.videoHeight = videoWidth;
        }
        Log.i("bitrate_before", videoBitRate+" "+OUTPUT_VIDEO_BIT_RATE);
        if (videoBitRate < OUTPUT_VIDEO_BIT_RATE) {
            OUTPUT_VIDEO_BIT_RATE = videoBitRate;
        }
        setSize(this.videoWidht, this.videoHeight);
        setSource(uri);
        setCopyAudio();
        setCopyVideo();
        TestWrapper.runTest(this);
        return mOutputFile;
    }

    private static class TestWrapper implements Runnable {
        private Throwable mThrowable;
        private ExtractDecodeEditEncodeMuxTest mTest;

        private TestWrapper(ExtractDecodeEditEncodeMuxTest test) {
            mTest = test;
        }

        @Override
        public void run() {
            try {
                mTest.extractDecodeEditEncodeMux();
            } catch (Throwable th) {
                mThrowable = th;
            }
        }

        public static void runTest(ExtractDecodeEditEncodeMuxTest test)
                throws Throwable {
            test.setOutputFile();
            TestWrapper wrapper = new TestWrapper(test);
            Thread th = new Thread(wrapper, "codec test");
            th.start();
            th.join();
            if (wrapper.mThrowable != null) {
                throw wrapper.mThrowable;
            }
        }
    }

    private void setCopyVideo() {
        mCopyVideo = true;
    }

    private void setCopyAudio() {
        mCopyAudio = isHaveSound;
    }

    private void setSize(int width, int height) {
        while (width % 16 != 0) {
            width--;
        }
        while (height % 16 != 0) {
            height--;
        }

        if ((width % 16) != 0 || (height % 16) != 0) {
            Log.w(TAG, "WARNING: width or height not multiple of 16");
        }
        mWidth = width;
        mHeight = height;
    }

    private void setSource(String uri) {
        mUri = uri;
    }

    private void setOutputFile() {
        mOutputFile = VideoUtils.getOutputMediaFile(Constant.MEDIA_TYPE_VIDEO).toString();
    }

    private int extractorInputChannelNum, decoderOutputChannelNum;

    private int extractorAudioSampleRate, decoderOutputAudioSampleRate;

    private void extractDecodeEditEncodeMux() throws Exception {
        Exception exception = null;

        MediaCodecInfo videoCodecInfo = selectCodec(OUTPUT_VIDEO_MIME_TYPE);
        MediaCodecInfo audioCodecInfo = selectCodec(OUTPUT_AUDIO_MIME_TYPE);

        MediaExtractor videoExtractor = null;
        MediaExtractor audioExtractor = null;
        InputSurface inputSurface = null;
        OutputSurface outputSurface = null;
        MediaCodec videoDecoder = null;
        MediaCodec audioDecoder = null;
        MediaCodec videoEncoder = null;
        MediaCodec audioEncoder = null;
        MediaMuxer muxer = null;

        try {
            if (mCopyVideo) {
                videoExtractor = createExtractor();
                int videoInputTrack = getAndSelectVideoTrackIndex(videoExtractor);

                MediaFormat inputFormat = videoExtractor.getTrackFormat(videoInputTrack);
                Log.i("extractor_vid_in_format", inputFormat + "");

                if(inputFormat.containsKey(MediaFormat.KEY_FRAME_RATE))
                {
                    originalVideoFrameRate = inputFormat.getInteger(MediaFormat.KEY_FRAME_RATE);
                    timestampConstant = 1000000/originalVideoFrameRate;
                }
                else
                {
                    videoExtractor.seekTo(33333, MediaExtractor.SEEK_TO_CLOSEST_SYNC);
                    videoExtractor.seekTo(0, MediaExtractor.SEEK_TO_CLOSEST_SYNC);
                }

                MediaFormat outputVideoFormat = MediaFormat.createVideoFormat(
                        OUTPUT_VIDEO_MIME_TYPE, mWidth, mHeight);
                outputVideoFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                        OUTPUT_VIDEO_COLOR_FORMAT);
                outputVideoFormat.setInteger(MediaFormat.KEY_BIT_RATE, OUTPUT_VIDEO_BIT_RATE);
                //outputVideoFormat.setInteger(MediaFormat.KEY_BIT_RATE, testBitRate);
                outputVideoFormat.setInteger(MediaFormat.KEY_FRAME_RATE, OUTPUT_VIDEO_FRAME_RATE);
                outputVideoFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL,
                        OUTPUT_VIDEO_IFRAME_INTERVAL);

                if (VERBOSE)
                    Log.d(TAG, "projected_encoder_vid_out_format: " + outputVideoFormat);

                AtomicReference<Surface> inputSurfaceReference = new AtomicReference<Surface>();

                videoEncoder = createVideoEncoder(videoCodecInfo,
                        outputVideoFormat, inputSurfaceReference);

                inputSurface = new InputSurface(inputSurfaceReference.get());
                inputSurface.makeCurrent();

                outputSurface = new OutputSurface(mContext);
                outputSurface.changeFragmentShader(FRAGMENT_SHADER);
                outputSurface.preloadFilterAssets(VideoUtils.getFilterAssets(themePos));
                //outputSurface.preloadValues(valuesToPreload);

                videoDecoder = createVideoDecoder(inputFormat, outputSurface.getSurface());
            }

            if (mCopyAudio) {
                audioExtractor = createExtractor();
                int audioInputTrack = getAndSelectAudioTrackIndex(audioExtractor);
                MediaFormat inputFormat = audioExtractor.getTrackFormat(audioInputTrack);
                Log.i("extractor_in_format", inputFormat+"");
                extractorAudioSampleRate = inputFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE);
                extractorInputChannelNum = inputFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT);

                OUTPUT_AUDIO_CHANNEL_COUNT = inputFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
                Log.d(TAG, "channel count" + OUTPUT_AUDIO_CHANNEL_COUNT);
                try {
                    sampleRate = inputFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE);
                    Log.d(TAG, "sampleRate:" + inputFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                MediaFormat outputAudioFormat = MediaFormat.createAudioFormat(OUTPUT_AUDIO_MIME_TYPE, sampleRate, OUTPUT_AUDIO_CHANNEL_COUNT);
                outputAudioFormat.setInteger(MediaFormat.KEY_BIT_RATE, OUTPUT_AUDIO_BIT_RATE);
                outputAudioFormat.setInteger(MediaFormat.KEY_AAC_PROFILE, OUTPUT_AUDIO_AAC_PROFILE);

                audioEncoder = createAudioEncoder(audioCodecInfo,
                        outputAudioFormat);
                audioDecoder = createAudioDecoder(inputFormat);
            }

            muxer = createMuxer();

            doExtractDecodeEditEncodeMux(videoExtractor, audioExtractor,
                    videoDecoder, videoEncoder, audioDecoder, audioEncoder,
                    muxer, inputSurface, outputSurface, viewportX, viewportY);
        } finally {
            if (VERBOSE)
                Log.d(TAG, "releasing extractor, decoder, encoder, and muxer");
            try {
                if (videoExtractor != null) {
                    videoExtractor.release();
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing videoExtractor", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (audioExtractor != null) {
                    audioExtractor.release();
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing audioExtractor", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (videoDecoder != null) {
                    videoDecoder.stop();
                    videoDecoder.release();
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing videoDecoder", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (outputSurface != null) {
                    outputSurface.release();
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing outputSurface", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (videoEncoder != null) {
                    videoEncoder.stop();
                    videoEncoder.release();
                }

            } catch (Exception e) {
                Log.e(TAG, "error while releasing videoEncoder", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (audioDecoder != null) {
                    audioDecoder.stop();
                    audioDecoder.release();
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing audioDecoder", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (audioEncoder != null) {
                    audioEncoder.stop();
                    audioEncoder.release();
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing audioEncoder", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (muxer != null) {
                    muxer.stop();
                    muxer.release();
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing muxer", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (inputSurface != null) {
                    inputSurface.release();
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing inputSurface", e);
                if (exception == null) {
                    exception = e;
                }
            }
        }
        if (exception != null) {
            throw exception;
        }
    }

    private MediaExtractor createExtractor() throws IOException {
        MediaExtractor extractor;
        extractor = new MediaExtractor();
        extractor.setDataSource(mUri);
        return extractor;
    }

    private MediaCodec createVideoDecoder(MediaFormat inputFormat,
                                          Surface surface) throws IOException {
        MediaCodec decoder = MediaCodec.createDecoderByType(getMimeTypeFor(inputFormat));
        decoder.configure(inputFormat, surface, null, 0);
        decoder.start();
        return decoder;
    }


    private MediaCodec createVideoEncoder(MediaCodecInfo codecInfo,
                                          MediaFormat format, AtomicReference<Surface> surfaceReference) throws IOException {
        MediaCodec encoder = MediaCodec.createByCodecName(codecInfo.getName());
        encoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        surfaceReference.set(encoder.createInputSurface());
        encoder.start();
        return encoder;
    }

    private MediaCodec createAudioDecoder(MediaFormat inputFormat) throws IOException {
        MediaCodec decoder = MediaCodec.createDecoderByType(getMimeTypeFor(inputFormat));
        decoder.configure(inputFormat, null, null, 0);
        decoder.start();
        return decoder;
    }

    private MediaCodec createAudioEncoder(MediaCodecInfo codecInfo,
                                          MediaFormat format) throws IOException {
        MediaCodec encoder = MediaCodec.createByCodecName(codecInfo.getName());
        encoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        encoder.start();
        return encoder;
    }

    private MediaMuxer createMuxer() throws IOException {
        return new MediaMuxer(mOutputFile,
                MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
    }

    private int getAndSelectVideoTrackIndex(MediaExtractor extractor) {
        for (int index = 0; index < extractor.getTrackCount(); ++index) {
            if (VERBOSE) {
                Log.d(TAG, "format for track " + index + " is "
                        + getMimeTypeFor(extractor.getTrackFormat(index)));
            }
            if (isVideoFormat(extractor.getTrackFormat(index))) {
                extractor.selectTrack(index);
                return index;
            }
        }
        return -1;
    }

    private int getAndSelectAudioTrackIndex(MediaExtractor extractor) {
        for (int index = 0; index < extractor.getTrackCount(); ++index) {
            if (VERBOSE) {
                Log.d(TAG, "format for track " + index + " is " + getMimeTypeFor(extractor.getTrackFormat(index)));
            }
            if (isAudioFormat(extractor.getTrackFormat(index))) {
                extractor.selectTrack(index);
                return index;
            }
        }
        return -1;
    }

    private void doExtractDecodeEditEncodeMux(MediaExtractor videoExtractor,
                                              MediaExtractor audioExtractor, MediaCodec videoDecoder,
                                              MediaCodec videoEncoder, MediaCodec audioDecoder,
                                              MediaCodec audioEncoder, MediaMuxer muxer,
                                              InputSurface inputSurface, OutputSurface outputSurface,
                                              float viewportX, float viewportY) {
        int gifFrameCount = 0;

        ByteBuffer[] videoDecoderInputBuffers = null;
        ByteBuffer[] videoDecoderOutputBuffers = null;
        ByteBuffer[] videoEncoderOutputBuffers = null;
        MediaCodec.BufferInfo videoDecoderOutputBufferInfo = null;
        MediaCodec.BufferInfo videoEncoderOutputBufferInfo = null;
        if (mCopyVideo) {
            videoDecoderInputBuffers = videoDecoder.getInputBuffers();
            videoDecoderOutputBuffers = videoDecoder.getOutputBuffers();
            videoEncoderOutputBuffers = videoEncoder.getOutputBuffers();
            videoDecoderOutputBufferInfo = new MediaCodec.BufferInfo();
            videoEncoderOutputBufferInfo = new MediaCodec.BufferInfo();
        }
        ByteBuffer[] audioDecoderInputBuffers = null;
        ByteBuffer[] audioDecoderOutputBuffers = null;
        ByteBuffer[] audioEncoderInputBuffers = null;
        ByteBuffer[] audioEncoderOutputBuffers = null;
        MediaCodec.BufferInfo audioDecoderOutputBufferInfo = null;
        MediaCodec.BufferInfo audioEncoderOutputBufferInfo = null;
        if (mCopyAudio) {
            audioDecoderInputBuffers = audioDecoder.getInputBuffers();
            audioDecoderOutputBuffers = audioDecoder.getOutputBuffers();
            audioEncoderInputBuffers = audioEncoder.getInputBuffers();
            audioEncoderOutputBuffers = audioEncoder.getOutputBuffers();
            audioDecoderOutputBufferInfo = new MediaCodec.BufferInfo();
            audioEncoderOutputBufferInfo = new MediaCodec.BufferInfo();
        }
        MediaFormat decoderOutputVideoFormat = null;
        MediaFormat decoderOutputAudioFormat = null;
        MediaFormat encoderOutputVideoFormat = null;
        MediaFormat encoderOutputAudioFormat = null;
        int outputVideoTrack = -1;
        int outputAudioTrack = -1;
        boolean videoExtractorDone = false;
        boolean videoDecoderDone = false;
        boolean videoEncoderDone = false;
        boolean audioExtractorDone = false;
        boolean audioDecoderDone = false;
        boolean audioEncoderDone = false;
        int pendingAudioDecoderOutputBufferIndex = -1;

        boolean muxing = false;

        int videoExtractedFrameCount = 0;
        int videoDecodedFrameCount = 0;
        int videoEncodedFrameCount = 0;

        int audioExtractedFrameCount = 0;
        int audioDecodedFrameCount = 0;
        int audioEncodedFrameCount = 0;

        while ((mCopyVideo && !videoEncoderDone)
                || (mCopyAudio && !audioEncoderDone)) {
            if (VERBOSE) {
                Log.d(TAG, String.format("loop: "

                                + "V(%b){" + "extracted:%d(doneMenuClick:%b) " + "decoded:%d(doneMenuClick:%b) "
                                + "encoded:%d(doneMenuClick:%b)} "

                                + "A(%b){" + "extracted:%d(doneMenuClick:%b) "
                                + "decoded:%d(doneMenuClick:%b) " + "encoded:%d(doneMenuClick:%b) "
                                + "pending:%d} "

                                + "muxing:%b(V:%d,A:%d)",

                        mCopyVideo, videoExtractedFrameCount, videoExtractorDone,
                        videoDecodedFrameCount, videoDecoderDone,
                        videoEncodedFrameCount, videoEncoderDone,

                        mCopyAudio, audioExtractedFrameCount,
                        audioExtractorDone, audioDecodedFrameCount,
                        audioDecoderDone, audioEncodedFrameCount,
                        audioEncoderDone, pendingAudioDecoderOutputBufferIndex,

                        muxing, outputVideoTrack, outputAudioTrack));
            }

            while (mCopyVideo && !videoExtractorDone
                    && (encoderOutputVideoFormat == null || muxing)) {
                int decoderInputBufferIndex = videoDecoder
                        .dequeueInputBuffer(TIMEOUT_USEC);
                if (decoderInputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    if (VERBOSE)
                        Log.d(TAG, "no video decoder input buffer");
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "video decoder: returned input buffer: "
                            + decoderInputBufferIndex);
                }
                ByteBuffer decoderInputBuffer = videoDecoderInputBuffers[decoderInputBufferIndex];
                int size = videoExtractor.readSampleData(decoderInputBuffer, 0);
                long presentationTime = videoExtractor.getSampleTime();
                if (VERBOSE) {
                    Log.d(TAG, "video extractor: returned buffer of size "
                            + size);
                    Log.d(TAG, "video extractor: returned buffer for time "
                            + presentationTime);
                }
                if (size >= 0) {
                    videoDecoder.queueInputBuffer(decoderInputBufferIndex, 0,
                            size, presentationTime,
                            videoExtractor.getSampleFlags());
                }
                videoExtractorDone = !videoExtractor.advance();
                if (videoExtractorDone) {
                    if (VERBOSE)
                        Log.d(TAG, "video extractor: EOS");
                    videoDecoder.queueInputBuffer(decoderInputBufferIndex, 0,
                            0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                }
                videoExtractedFrameCount++;
                break;
            }


            while (mCopyAudio && !audioExtractorDone && (encoderOutputAudioFormat == null || muxing)) {
                int decoderInputBufferIndex = audioDecoder.dequeueInputBuffer(TIMEOUT_USEC);
                if (decoderInputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    if (VERBOSE)
                        Log.d(TAG, "no audio decoder input buffer");
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "audio decoder: returned input buffer: "
                            + decoderInputBufferIndex);
                }
                ByteBuffer decoderInputBuffer = audioDecoderInputBuffers[decoderInputBufferIndex];
                int size = audioExtractor.readSampleData(decoderInputBuffer, 0);
                long presentationTime = audioExtractor.getSampleTime();
                if (VERBOSE) {
                    Log.d(TAG, "audio extractor: returned buffer of size "
                            + size);
                    Log.d(TAG, "audio extractor: returned buffer for time "
                            + presentationTime);
                }
                if (size >= 0) {
                    audioDecoder.queueInputBuffer(decoderInputBufferIndex, 0,
                            size, presentationTime,
                            audioExtractor.getSampleFlags());
                }
                audioExtractorDone = !audioExtractor.advance();
                if (audioExtractorDone) {
                    if (VERBOSE)
                        Log.d(TAG, "audio extractor: EOS");
                    audioDecoder.queueInputBuffer(decoderInputBufferIndex, 0,
                            0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                }
                audioExtractedFrameCount++;
                break;
            }

            while (mCopyVideo && !videoDecoderDone
                    && (encoderOutputVideoFormat == null || muxing)) {
                int decoderOutputBufferIndex = videoDecoder
                        .dequeueOutputBuffer(videoDecoderOutputBufferInfo,
                                TIMEOUT_USEC);
                if (decoderOutputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    if (VERBOSE)
                        Log.d(TAG, "no video decoder output buffer");
                    break;
                }
                if (decoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                    if (VERBOSE)
                        Log.d(TAG, "video decoder: output buffers changed");
                    videoDecoderOutputBuffers = videoDecoder.getOutputBuffers();
                    break;
                }
                if (decoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                    decoderOutputVideoFormat = videoDecoder.getOutputFormat();
                    Log.i("decoder_vid_out_format", decoderOutputVideoFormat+"");
                    if (VERBOSE) {
                        Log.d(TAG, "video decoder: output format changed: "
                                + decoderOutputVideoFormat);
                    }
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "video decoder: returned output buffer: "
                            + decoderOutputBufferIndex);
                    Log.d(TAG, "video decoder: returned buffer of size "
                            + videoDecoderOutputBufferInfo.size);
                    Log.d(TAG, "video decoder: returned buffer for time "
                            + videoDecoderOutputBufferInfo.presentationTimeUs);
                }
                ByteBuffer decoderOutputBuffer = videoDecoderOutputBuffers[decoderOutputBufferIndex];
                if ((videoDecoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    if (VERBOSE)
                        Log.d(TAG, "video decoder: codec config buffer");
                    videoDecoder.releaseOutputBuffer(decoderOutputBufferIndex,
                            false);
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "video decoder: returned buffer for time "
                            + videoDecoderOutputBufferInfo.presentationTimeUs);
                }
                boolean render = videoDecoderOutputBufferInfo.size != 0;
                videoDecoder.releaseOutputBuffer(decoderOutputBufferIndex,
                        render);
                if (render) {
                    if (VERBOSE)
                        Log.d(TAG, "output surface: await new image");
                    outputSurface.awaitNewImage();
                    if (VERBOSE)
                        Log.d(TAG, "output surface: draw image");

                    outputSurface.drawImage();

                    inputSurface
                            .setPresentationTime(videoDecoderOutputBufferInfo.presentationTimeUs * 1000);
                    if (VERBOSE)
                        Log.d(TAG, "input surface: swap buffers");
                    inputSurface.swapBuffers();
                    if (VERBOSE)
                        Log.d(TAG, "video encoder: notified of new frame");
                }
                videoDecodedFrameCount++;
                if ((videoDecoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    if (VERBOSE)
                        Log.d(TAG, "video decoder: EOS");
                    videoDecoderDone = true;
                    videoEncoder.signalEndOfInputStream();
                }
                break;
            }

            while (mCopyAudio && !audioDecoderDone
                    && pendingAudioDecoderOutputBufferIndex == -1
                    && (encoderOutputAudioFormat == null || muxing)) {
                int decoderOutputBufferIndex = audioDecoder
                        .dequeueOutputBuffer(audioDecoderOutputBufferInfo,
                                TIMEOUT_USEC);
                Log.i("decoder_out_buf_info", audioDecoderOutputBufferInfo.size + " " + audioDecoderOutputBufferInfo.offset);
                if (decoderOutputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    if (VERBOSE)
                        Log.d(TAG, "no audio decoder output buffer");
                    break;
                }
                if (decoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                    if (VERBOSE)
                        Log.d(TAG, "audio decoder: output buffers changed");
                    audioDecoderOutputBuffers = audioDecoder.getOutputBuffers();
                    break;
                }
                if (decoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                    decoderOutputAudioFormat = audioDecoder.getOutputFormat();
                    decoderOutputChannelNum = decoderOutputAudioFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
                    decoderOutputAudioSampleRate = decoderOutputAudioFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE);
                    Log.i("decoder_out_format", decoderOutputAudioFormat+"");
                    if (VERBOSE) {
                        Log.d(TAG, "audio decoder: output format changed: "
                                + decoderOutputAudioFormat);
                    }
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "audio decoder: returned output buffer: "
                            + decoderOutputBufferIndex);
                }
                if (VERBOSE) {
                    Log.d(TAG, "audio decoder: returned buffer of size "
                            + audioDecoderOutputBufferInfo.size);
                }
                ByteBuffer decoderOutputBuffer = audioDecoderOutputBuffers[decoderOutputBufferIndex];
                if ((audioDecoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    if (VERBOSE)
                        Log.d(TAG, "audio decoder: codec config buffer");
                    audioDecoder.releaseOutputBuffer(decoderOutputBufferIndex,
                            false);
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "audio decoder: returned buffer for time "
                            + audioDecoderOutputBufferInfo.presentationTimeUs);
                }
                if (VERBOSE) {
                    Log.d(TAG, "audio decoder: output buffer is now pending: "
                            + pendingAudioDecoderOutputBufferIndex);
                }
                pendingAudioDecoderOutputBufferIndex = decoderOutputBufferIndex;
                audioDecodedFrameCount++;
                break;
            }

            while (mCopyAudio && pendingAudioDecoderOutputBufferIndex != -1) {
                if (VERBOSE) {
                    Log.d(TAG,
                            "audio decoder: attempting to process pending buffer: "
                                    + pendingAudioDecoderOutputBufferIndex);
                }
                int encoderInputBufferIndex = audioEncoder
                        .dequeueInputBuffer(TIMEOUT_USEC);
                if (encoderInputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    if (VERBOSE)
                        Log.d(TAG, "no audio encoder input buffer");
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "audio encoder: returned input buffer: "
                            + encoderInputBufferIndex);
                }
                ByteBuffer encoderInputBuffer = audioEncoderInputBuffers[encoderInputBufferIndex];
                int size = audioDecoderOutputBufferInfo.size;
                long presentationTime = audioDecoderOutputBufferInfo.presentationTimeUs;
                if (VERBOSE) {
                    Log.d(TAG, "audio decoder: processing pending buffer: "
                            + pendingAudioDecoderOutputBufferIndex);
                }
                if (VERBOSE) {
                    Log.d(TAG, "audio decoder: pending buffer of size " + size);
                    Log.d(TAG, "audio decoder: pending buffer for time "
                            + presentationTime);
                }
                if (size >= 0) {
                    ByteBuffer decoderOutputBuffer = audioDecoderOutputBuffers[pendingAudioDecoderOutputBufferIndex]
                            .duplicate();

                    byte[] testBufferContents = new byte[size];
                    //int bufferSize = (extractorInputChannelNum == 1 && decoderOutputChannelNum == 2) ? size / 2 : size;
                    float samplingFactor = (decoderOutputChannelNum/extractorInputChannelNum) * (decoderOutputAudioSampleRate / extractorAudioSampleRate);
                    int bufferSize = size / (int)samplingFactor;
                    Log.i("sampling_factor", samplingFactor+" "+bufferSize);

                    if (decoderOutputBuffer.remaining() < size) {
                        for (int i = decoderOutputBuffer.remaining(); i < size; i++) {
                            testBufferContents[i] = 0;  // pad with extra 0s to make a full frame.
                        }
                        decoderOutputBuffer.get(testBufferContents, 0, decoderOutputBuffer.remaining());
                    } else {
                        decoderOutputBuffer.get(testBufferContents, 0, size);
                    }

                    //WARNING: This works for 11025-22050-44100 or 8000-16000-24000-48000
                    //What about in-between?
                    //BTW, the size of the bytebuffer may be less than 4096 depending on the sampling factor
                    //(Now that I think about it I should've realized this back when I decoded the video result from the encoding - 2048 bytes decoded)
                    if (((int)samplingFactor) > 1) {
                        Log.i("s2m_conversion", "Stereo to Mono and/or downsampling");
                        byte[] finalByteBufferContent = new byte[size / 2];

                        for (int i = 0; i < bufferSize; i+=2) {
                            if((i+1)*((int)samplingFactor) > testBufferContents.length)
                            {
                                finalByteBufferContent[i] = 0;
                                finalByteBufferContent[i+1] = 0;
                            }
                            else
                            {
                                finalByteBufferContent[i] = testBufferContents[i*((int)samplingFactor)];
                                finalByteBufferContent[i+1] = testBufferContents[i*((int)samplingFactor) + 1];
                            }
                        }

                        decoderOutputBuffer = ByteBuffer.wrap(finalByteBufferContent);
                    }

                    decoderOutputBuffer.position(audioDecoderOutputBufferInfo.offset);
                    decoderOutputBuffer.limit(audioDecoderOutputBufferInfo.offset + bufferSize);
                    //decoderOutputBuffer.limit(audioDecoderOutputBufferInfo.offset + size);
                    encoderInputBuffer.position(0);

                    encoderInputBuffer.put(decoderOutputBuffer);

                    audioEncoder.queueInputBuffer(encoderInputBufferIndex, 0, bufferSize, presentationTime, audioDecoderOutputBufferInfo.flags);
                    //audioEncoder.queueInputBuffer(encoderInputBufferIndex, 0, size, presentationTime, audioDecoderOutputBufferInfo.flags);
                }
                audioDecoder.releaseOutputBuffer(
                        pendingAudioDecoderOutputBufferIndex, false);
                pendingAudioDecoderOutputBufferIndex = -1;
                if ((audioDecoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    if (VERBOSE)
                        Log.d(TAG, "audio decoder: EOS");
                    audioDecoderDone = true;
                }
                break;
            }

            while (mCopyVideo && !videoEncoderDone
                    && (encoderOutputVideoFormat == null || muxing)) {

                int encoderOutputBufferIndex = videoEncoder.dequeueOutputBuffer(videoEncoderOutputBufferInfo, TIMEOUT_USEC);

                if (encoderOutputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    if (VERBOSE)
                        Log.d(TAG, "no video encoder output buffer");
                    break;
                }
                if (encoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                    if (VERBOSE)
                        Log.d(TAG, "video encoder: output buffers changed");
                    videoEncoderOutputBuffers = videoEncoder.getOutputBuffers();
                    break;
                }
                if (encoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                    if (VERBOSE)
                        Log.d(TAG, "video encoder: output format changed");
                    if (outputVideoTrack >= 0) {
                        Log.d(TAG,
                                "video encoder changed its output format again?");
                    }
                    encoderOutputVideoFormat = videoEncoder.getOutputFormat();
                    Log.i("encoder_vid_out_format", encoderOutputVideoFormat + "");
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "video encoder: returned output buffer: "
                            + encoderOutputBufferIndex);
                    Log.d(TAG, "video encoder: returned buffer of size "
                            + videoEncoderOutputBufferInfo.size);
                }
                ByteBuffer encoderOutputBuffer = videoEncoderOutputBuffers[encoderOutputBufferIndex];
                if ((videoEncoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    if (VERBOSE)
                        Log.d(TAG, "video encoder: codec config buffer");
                    videoEncoder.releaseOutputBuffer(encoderOutputBufferIndex,
                            false);
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "video encoder: returned buffer for time "
                            + videoEncoderOutputBufferInfo.presentationTimeUs);
                }

                if (videoEncoderOutputBufferInfo.size != 0) {
                    muxer.writeSampleData(outputVideoTrack,
                            encoderOutputBuffer, videoEncoderOutputBufferInfo);
                }
                if ((videoEncoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    if (VERBOSE)
                        Log.d(TAG, "video encoder: EOS");
                    videoEncoderDone = true;
                }
                videoEncoder.releaseOutputBuffer(encoderOutputBufferIndex,
                        false);
                videoEncodedFrameCount++;
                break;
            }

            while (mCopyAudio && !audioEncoderDone
                    && (encoderOutputAudioFormat == null || muxing)) {
                int encoderOutputBufferIndex = audioEncoder
                        .dequeueOutputBuffer(audioEncoderOutputBufferInfo,
                                TIMEOUT_USEC);
                if (encoderOutputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    if (VERBOSE)
                        Log.d(TAG, "no audio encoder output buffer");
                    break;
                }
                if (encoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                    if (VERBOSE)
                        Log.d(TAG, "audio encoder: output buffers changed");
                    audioEncoderOutputBuffers = audioEncoder.getOutputBuffers();
                    break;
                }
                if (encoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                    if (VERBOSE)
                        Log.d(TAG, "audio encoder: output format changed");
                    if (outputAudioTrack >= 0) {
                        Log.d(TAG,
                                "audio encoder changed its output format again?");
                    }

                    encoderOutputAudioFormat = audioEncoder.getOutputFormat();
                    Log.i("encoder_out_format", encoderOutputAudioFormat + "");
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "audio encoder: returned output buffer: "
                            + encoderOutputBufferIndex);
                    Log.d(TAG, "audio encoder: returned buffer of size "
                            + audioEncoderOutputBufferInfo.size);
                }
                ByteBuffer encoderOutputBuffer = audioEncoderOutputBuffers[encoderOutputBufferIndex];
                if ((audioEncoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    if (VERBOSE)
                        Log.d(TAG, "audio encoder: codec config buffer");
                    audioEncoder.releaseOutputBuffer(encoderOutputBufferIndex,
                            false);
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "audio encoder: returned buffer for time "
                            + audioEncoderOutputBufferInfo.presentationTimeUs);
                }
                if (audioEncoderOutputBufferInfo.size != 0) {
                    muxer.writeSampleData(outputAudioTrack,
                            encoderOutputBuffer, audioEncoderOutputBufferInfo);
                }
                if ((audioEncoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    if (VERBOSE)
                        Log.d(TAG, "audio encoder: EOS");
                    audioEncoderDone = true;
                }
                audioEncoder.releaseOutputBuffer(encoderOutputBufferIndex,
                        false);
                audioEncodedFrameCount++;
                break;
            }

            if (!muxing && (!mCopyAudio || encoderOutputAudioFormat != null)
                    && (!mCopyVideo || encoderOutputVideoFormat != null)) {
                if (mCopyVideo) {
                    Log.d(TAG, "muxer: adding video track.");
                    outputVideoTrack = muxer.addTrack(encoderOutputVideoFormat);
                }
                if (mCopyAudio) {
                    Log.d(TAG, "muxer: adding audio track.");
                    outputAudioTrack = muxer.addTrack(encoderOutputAudioFormat);
                }
                Log.d(TAG, "muxer: starting");
                muxer.start();
                muxing = true;
            }
        }
    }

    private static boolean isVideoFormat(MediaFormat format) {
        return getMimeTypeFor(format).startsWith("video/");
    }

    private static boolean isAudioFormat(MediaFormat format) {
        return getMimeTypeFor(format).startsWith("audio/");
    }

    private static String getMimeTypeFor(MediaFormat format) {
        return format.getString(MediaFormat.KEY_MIME);
    }

    private static MediaCodecInfo selectCodec(String mimeType) {
        int numCodecs = MediaCodecList.getCodecCount();
        for (int i = 0; i < numCodecs; i++) {
            MediaCodecInfo codecInfo = MediaCodecList.getCodecInfoAt(i);
            if (!codecInfo.isEncoder()) {
                continue;
            }
            String[] types = codecInfo.getSupportedTypes();
            for (int j = 0; j < types.length; j++) {
                if (types[j].equalsIgnoreCase(mimeType)) {
                    return codecInfo;
                }
            }
        }
        return null;
    }


    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }


}
