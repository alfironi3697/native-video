//TODO: properly apply silence when video has no audio track

package com.abcode.nativevideo.codec;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.media.MediaMuxer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Surface;

import com.abcode.nativevideo.utils.Constant;
import com.abcode.nativevideo.utils.VideoUtils;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by alfironi on 6/26/2015.
 */
@TargetApi(18)
public class ConcatenateSequentialTest {

    private static final String TAG = ConcatenateSequentialTest.class
            .getSimpleName();

    private int rate_gifVideo = 0;
    private static boolean VERBOSE = true;
    private static final int TIMEOUT_USEC = 0;
    private static final String OUTPUT_VIDEO_MIME_TYPE = "video/avc";
    private static int OUTPUT_VIDEO_BIT_RATE = 3200000;
    private static int OUTPUT_VIDEO_FRAME_RATE = 30;
    private static final int OUTPUT_VIDEO_IFRAME_INTERVAL = 5;
    private static final int OUTPUT_VIDEO_COLOR_FORMAT = MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface;

    int videoProgress = 0, audioProgress = 0;

    // parameters for the audio encoder
    private static final String OUTPUT_AUDIO_MIME_TYPE = "audio/mp4a-latm";
    private static int OUTPUT_AUDIO_CHANNEL_COUNT = 2;
    private static int OUTPUT_AUDIO_BIT_RATE = 192 * 1024;
    private static int OUTPUT_AUDIO_SAMPLE_RATE = 96000;
    private static final int OUTPUT_AUDIO_AAC_PROFILE = MediaCodecInfo.CodecProfileLevel.AACObjectLC;

    private Context mContext;
    private static String FRAGMENT_SHADER = "";
    private boolean mCopyVideo;
    private boolean mCopyAudio;
    private int mWidth = -1;
    private int mHeight = -1;
    private String mUri;
    private String mOutputFile;
    private boolean status;
    private boolean isHaveSound;
    private boolean isFromRecord;
    private boolean isFit;
    private float density;
    private int mScreenHeight, videoWidht, videoHeight, orientation;
    private float[] valuesToPreload;

    private int originalVideoFrameRate;
    private int timestampConstant;

    private ProgressDialog progIndicator;

    private long prevEncoderPresTime = 0;

    private ArrayList<float[]> offsetList;
    private float[] thisIterationOffsets;

    private float viewportX, viewportY;

    private ArrayList<String> mUriList;
    private int themePos;

    private long totalTime;

    public ConcatenateSequentialTest(Context context) {
        mContext = context;
    }

    public String testExtractDecodeEditEncodeMuxAudioVideo(ArrayList<String> uriList, int position, boolean status, float density, int mScreenHeight,
                                                           boolean isHaveSound, boolean isFromRecord, int videoWidth, int videoHeight,
                                                           int orientation, boolean isFit, ArrayList<float[]> offsetList, float[] valuesToPreload,
                                                           @Nullable ProgressDialog progressIndicatorDialog)
            throws Throwable {

        OUTPUT_VIDEO_BIT_RATE = 2000000;

        progIndicator = progressIndicatorDialog;

        float TARGET_SIZE = 720f;
        prevEncoderPresTime = 0;
        FRAGMENT_SHADER = VideoUtils.getPrelimFragmentShader(mContext, position);
        themePos = position;
        if (isFit) {
            if (orientation == 90 || orientation == 270) {
                float width = (float) videoHeight * (TARGET_SIZE / (float) videoWidth);
                float height = TARGET_SIZE;
                setSize((int) width, (int) height);
            } else {
                if (videoWidth > videoHeight) {
                    //landscape video
                    float width = TARGET_SIZE;
                    float height = (float) videoHeight * (TARGET_SIZE / (float) videoWidth);
                    setSize((int) width, (int) height);
                } else {
                    //portrait video
                    float width = (float) videoWidth * (TARGET_SIZE / (float) videoHeight);
                    float height = TARGET_SIZE;
                    setSize((int) width, (int) height);
                }

            }
        } else {
            setSize((int) TARGET_SIZE, (int) TARGET_SIZE);
        }
        this.orientation = orientation;
        this.status = status;
        this.density = density;
        this.isFit = isFit;
        this.mScreenHeight = mScreenHeight;
        this.isFromRecord = isFromRecord;
        this.videoWidht = videoWidth;
        this.videoHeight = videoHeight;
        this.mUriList = uriList;
        this.offsetList = offsetList;
        this.valuesToPreload = valuesToPreload;
        this.isHaveSound = mUriList.size() > 1 || isHaveSound;

        this.totalTime = calcTotalDurationInNs();

        OUTPUT_VIDEO_BIT_RATE = Build.VERSION.SDK_INT < 19 ? calculateAverageBitrate() : scaleDownBitrate(0, 480);

        setSource(mUriList.get(0));
        setCopyAudio();
        setCopyVideo();
        TestWrapper.runTest(this);
        return mOutputFile;
    }

    private static class TestWrapper implements Runnable {
        private Throwable mThrowable;
        private ConcatenateSequentialTest mTest;

        private TestWrapper(ConcatenateSequentialTest test) {
            mTest = test;
        }

        @Override
        public void run() {
            try {
                mTest.extractDecodeEditEncodeMux();
            } catch (Throwable th) {
                mThrowable = th;
            }
        }

        public static void runTest(ConcatenateSequentialTest test)
                throws Throwable {
            test.setOutputFile();
            TestWrapper wrapper = new TestWrapper(test);
            Thread th = new Thread(wrapper, "codec test");
            th.start();
            th.join();
            if (wrapper.mThrowable != null) {
                throw wrapper.mThrowable;
            }
        }
    }

    private void setCopyVideo() {
        mCopyVideo = true;
    }

    private void setCopyAudio() {
        mCopyAudio = isHaveSound;
    }

    private void setSize(int width, int height) {
        while (width % 16 != 0) {
            width--;
        }
        while (height % 16 != 0) {
            height--;
        }

        if ((width % 16) != 0 || (height % 16) != 0) {
            Log.w(TAG, "WARNING: width or height not multiple of 16");
        }
        mWidth = width;
        mHeight = height;
    }

    private void setSource(String uri) {
        mUri = uri;
    }

    private void setOutputFile() {
        mOutputFile = VideoUtils.getOutputMediaFile(Constant.MEDIA_TYPE_VIDEO).toString();
    }

    private int extractorInputChannelNum, decoderOutputChannelNum;

    private int extractorAudioSampleRate, decoderOutputAudioSampleRate;

    private int videoUriIndex, audioUriIndex;

    MediaExtractor videoExtractor = null;
    MediaExtractor audioExtractor = null;
    InputSurface inputSurface = null;
    OutputSurface outputSurface = null;
    MediaCodec videoDecoder = null;
    MediaCodec audioDecoder = null;
    MediaCodec videoEncoder = null;
    MediaCodec audioEncoder = null;
    MediaMuxer muxer = null;

    private void initializeVideoProcessLoop(int uriIndex) throws IOException
    {
        videoExtractorDone = false;
        videoDecoderDone = false;

        videoExtractor = createExtractor(uriIndex);
        int videoInputTrack = getAndSelectVideoTrackIndex(videoExtractor);

        MediaFormat inputFormat = videoExtractor.getTrackFormat(videoInputTrack);

        if(inputFormat.containsKey(MediaFormat.KEY_FRAME_RATE))
        {
            originalVideoFrameRate = inputFormat.getInteger(MediaFormat.KEY_FRAME_RATE);
            timestampConstant = 1000000/originalVideoFrameRate;
        }
        else
        {
            videoExtractor.seekTo(33333, MediaExtractor.SEEK_TO_CLOSEST_SYNC);
            videoExtractor.seekTo(0, MediaExtractor.SEEK_TO_CLOSEST_SYNC);
        }

        outputSurface = new OutputSurface(mContext);
        outputSurface.changeFragmentShader(FRAGMENT_SHADER);
        outputSurface.preloadFilterAssets(VideoUtils.getFilterAssets(themePos));
        outputSurface.preloadValues(valuesToPreload);

        videoDecoder = createVideoDecoder(inputFormat, outputSurface.getSurface());
        videoDecoderInputBuffers = this.videoDecoder.getInputBuffers();
        videoDecoderOutputBuffers = this.videoDecoder.getOutputBuffers();
        videoDecoderOutputBufferInfo = new MediaCodec.BufferInfo();

        if(videoEncoder != null && Build.VERSION.SDK_INT >= 19)
        {
            Bundle params = new Bundle();
            int bitrate = scaleDownBitrate(uriIndex, 480);
            params.putInt(MediaCodec.PARAMETER_KEY_VIDEO_BITRATE, bitrate);
            videoEncoder.setParameters(params);
        }
        if(!isFromRecord){thisIterationOffsets = offsetList.get(videoUriIndex);}
        else {
            thisIterationOffsets = new float[]{-1f, -1f};
        }
    }

    private int scaleDownBitrate(int uriIndex, int squareDim)
    {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(mUriList.get(uriIndex));
        int bitrate = Integer.valueOf(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE));
        int vidWidth = Integer.valueOf(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
        int vidHeight = Integer.valueOf(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
        if(vidWidth * vidHeight > squareDim*squareDim)
        {
            float ratio = (float)(squareDim*squareDim)/(vidWidth*vidHeight);
            bitrate *= ratio;
        }
        int lowerThreshold = (int)(720 * 720 * 30 * 0.105);
        int upperThreshold = 2400000;
        if(bitrate < lowerThreshold) {bitrate = lowerThreshold;}
        else if (bitrate > upperThreshold) {
            bitrate = upperThreshold;
        }
        return bitrate;
    }

    private int calculateAverageBitrate() throws IOException
    {
        long bitrateSum = 0;
        int threshold = (int)(720 * 720 * 30 * 0.07);
        for(int i = 0; i < mUriList.size(); i++)
        {
            int bitrate = scaleDownBitrate(videoUriIndex, 480);
            if(bitrate < threshold)
            {
                bitrate = threshold;
            }
            bitrateSum += bitrate;
        }
        return (int)((float)bitrateSum / mUriList.size());
    }

    private void calculateLowestSampleRateAndChannels() throws IOException
    {
        for(int i = 0; i < mUriList.size(); i++)
        {
            MediaExtractor instance = createExtractor(i);
            int audioInputTrack = getAndSelectAudioTrackIndex(instance);
            if(audioInputTrack > -1) {
                MediaFormat instanceInputFormat = instance.getTrackFormat(audioInputTrack);

                try {
                    int tempChannelCount = instanceInputFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
                    if (tempChannelCount < OUTPUT_AUDIO_CHANNEL_COUNT) {
                        OUTPUT_AUDIO_CHANNEL_COUNT = tempChannelCount;
                    }
                    int tempSampleRate = instanceInputFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE);
                    if (tempSampleRate < OUTPUT_AUDIO_SAMPLE_RATE) {
                        OUTPUT_AUDIO_SAMPLE_RATE = tempSampleRate;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            instance.release();
            instance = null;
        }
    }

    private long projectedTimestamp;
    private int silenceFrameCount;

    private boolean videoHasSound;

    private int calcNumOfAudioFrames(int index)
    {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(mUriList.get(index));
        String hasAudio = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_HAS_AUDIO);
        if(hasAudio == null) {
            long duration = Long.valueOf(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
            return (int)(duration*1000 / projectedTimestamp);
        }
        else
        {
            return -1;
        }
    }

    private long calcTotalDurationInNs() {
        long sum = 0;

        for(int i = 0; i < mUriList.size(); i++) {
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(mUriList.get(i));

            sum += Long.valueOf(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION))*1000;
        }

        Log.i("total_duration", sum+"");
        return sum;
    }

    private void initializeAudioProcessLoop(int index) throws IOException
    {
        audioExtractorDone = false;
        audioDecoderDone = false;

        projectedTimestamp = 1000000000/OUTPUT_AUDIO_SAMPLE_RATE;

        audioExtractor = createExtractor(index);
        int audioInputTrack = getAndSelectAudioTrackIndex(audioExtractor);
        videoHasSound = audioInputTrack > -1;
        if(videoHasSound) {
            MediaFormat inputFormat = audioExtractor.getTrackFormat(audioInputTrack);

            extractorAudioSampleRate = inputFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE);
            extractorInputChannelNum = inputFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT);

            this.audioDecoder = createAudioDecoder(inputFormat);

            audioDecoderInputBuffers = this.audioDecoder.getInputBuffers();
            audioDecoderOutputBuffers = this.audioDecoder.getOutputBuffers();
            audioDecoderOutputBufferInfo = new MediaCodec.BufferInfo();
        }
        else
        {
            audioDecoder = null;
        }
    }

    private void extractDecodeEditEncodeMux() throws Exception {
        Exception exception = null;

        videoUriIndex = 0;
        audioUriIndex = 0;

        MediaCodecInfo videoCodecInfo = selectCodec(OUTPUT_VIDEO_MIME_TYPE);
        MediaCodecInfo audioCodecInfo = selectCodec(OUTPUT_AUDIO_MIME_TYPE);

        videoExtractor = null;
        audioExtractor = null;
        inputSurface = null;
        outputSurface = null;
        videoDecoder = null;
        audioDecoder = null;
        videoEncoder = null;
        audioEncoder = null;
        muxer = null;

        try {
            if (mCopyVideo) {
                MediaFormat outputVideoFormat = MediaFormat.createVideoFormat(
                        OUTPUT_VIDEO_MIME_TYPE, mWidth, mHeight);
                outputVideoFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                        OUTPUT_VIDEO_COLOR_FORMAT);
                outputVideoFormat.setInteger(MediaFormat.KEY_BIT_RATE, OUTPUT_VIDEO_BIT_RATE);
                outputVideoFormat.setInteger(MediaFormat.KEY_FRAME_RATE, OUTPUT_VIDEO_FRAME_RATE);
                outputVideoFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL,
                        OUTPUT_VIDEO_IFRAME_INTERVAL);

                AtomicReference<Surface> inputSurfaceReference = new AtomicReference<Surface>();

                videoEncoder = createVideoEncoder(videoCodecInfo,
                        outputVideoFormat, inputSurfaceReference);

                inputSurface = new InputSurface(inputSurfaceReference.get());
                inputSurface.makeCurrent();

                initializeVideoProcessLoop(videoUriIndex); //Has to be put in the end because inputSurface initializes the EGL Context
            }

            if (mCopyAudio) {
                initializeAudioProcessLoop(audioUriIndex);

                calculateLowestSampleRateAndChannels();

                projectedTimestamp = 1000000000/OUTPUT_AUDIO_SAMPLE_RATE;

                silenceFrameCount = calcNumOfAudioFrames(audioUriIndex);

                MediaFormat outputAudioFormat = MediaFormat.createAudioFormat(OUTPUT_AUDIO_MIME_TYPE, OUTPUT_AUDIO_SAMPLE_RATE, OUTPUT_AUDIO_CHANNEL_COUNT);
                outputAudioFormat.setInteger(MediaFormat.KEY_BIT_RATE, OUTPUT_AUDIO_BIT_RATE);
                outputAudioFormat.setInteger(MediaFormat.KEY_AAC_PROFILE, OUTPUT_AUDIO_AAC_PROFILE);

                audioEncoder = createAudioEncoder(audioCodecInfo, outputAudioFormat);
            }

            muxer = createMuxer();

            if (status) {
                muxer.setOrientationHint(90);
            }

            doExtractDecodeEditEncodeMux(videoExtractor, audioExtractor,
                    videoDecoder, videoEncoder, audioDecoder, audioEncoder,
                    muxer, inputSurface, outputSurface, viewportX, viewportY);
        } finally {
            progIndicator.dismiss();
            if (VERBOSE)
                Log.d(TAG, "releasing extractor, decoder, encoder, and muxer");
            try {
                if (videoExtractor != null) {
                    videoExtractor.release();
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing videoExtractor", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (audioExtractor != null) {
                    audioExtractor.release();
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing audioExtractor", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (videoDecoder != null) {
                    videoDecoder.stop();
                    videoDecoder.release();
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing videoDecoder", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (outputSurface != null) {
                    outputSurface.resourceCleanup();
                    outputSurface.release();
                    outputSurface = null;
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing outputSurface", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (videoEncoder != null) {
                    videoEncoder.stop();
                    videoEncoder.release();
                }

            } catch (Exception e) {
                Log.e(TAG, "error while releasing videoEncoder", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (audioDecoder != null) {
                    audioDecoder.stop();
                    audioDecoder.release();
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing audioDecoder", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (audioEncoder != null) {
                    audioEncoder.stop();
                    audioEncoder.release();
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing audioEncoder", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (muxer != null) {
                    muxer.stop();
                    muxer.release();
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing muxer", e);
                if (exception == null) {
                    exception = e;
                }
            }
            try {
                if (inputSurface != null) {
                    inputSurface.release();
                }
            } catch (Exception e) {
                Log.e(TAG, "error while releasing inputSurface", e);
                if (exception == null) {
                    exception = e;
                }
            }
        }
        if (exception != null) {
            throw exception;
        }
    }

    protected String convertMediaUriToPath(Uri uri) {
        String [] proj={MediaStore.Video.Media.DATA};
        Cursor cursor = mContext.getContentResolver().query(uri, proj,  null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(column_index);
        cursor.close();
        return path;
    }

    private MediaExtractor createExtractor(int index) throws IOException {
        MediaExtractor extractor;
        extractor = new MediaExtractor();
        extractor.setDataSource(mUriList.get(index));
        //extractor.setDataSource(mContext, Uri.parse(mUriList.get(index)), null);
        return extractor;
    }

    private MediaCodec createVideoDecoder(MediaFormat inputFormat,
                                          Surface surface) throws IOException {
        MediaCodec decoder = MediaCodec.createDecoderByType(getMimeTypeFor(inputFormat));
        decoder.configure(inputFormat, surface, null, 0);
        decoder.start();
        return decoder;
    }


    private MediaCodec createVideoEncoder(MediaCodecInfo codecInfo,
                                          MediaFormat format, AtomicReference<Surface> surfaceReference) throws IOException {
        MediaCodec encoder = MediaCodec.createByCodecName(codecInfo.getName());
        encoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        surfaceReference.set(encoder.createInputSurface());
        encoder.start();
        return encoder;
    }

    private MediaCodec createAudioDecoder(MediaFormat inputFormat) throws IOException {
        MediaCodec decoder = MediaCodec.createDecoderByType(getMimeTypeFor(inputFormat));
        decoder.configure(inputFormat, null, null, 0);
        decoder.start();
        return decoder;
    }

    private MediaCodec createAudioEncoder(MediaCodecInfo codecInfo,
                                          MediaFormat format) throws IOException {
        MediaCodec encoder = MediaCodec.createByCodecName(codecInfo.getName());
        encoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        encoder.start();
        return encoder;
    }

    private MediaMuxer createMuxer() throws IOException {
        return new MediaMuxer(mOutputFile,
                MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
    }

    private int getAndSelectVideoTrackIndex(MediaExtractor extractor) {
        for (int index = 0; index < extractor.getTrackCount(); ++index) {
            if (VERBOSE) {
                Log.d(TAG, "format for track " + index + " is "
                        + getMimeTypeFor(extractor.getTrackFormat(index)));
            }
            if (isVideoFormat(extractor.getTrackFormat(index))) {
                extractor.selectTrack(index);
                return index;
            }
        }
        return -1;
    }

    private int getAndSelectAudioTrackIndex(MediaExtractor extractor) {
        for (int index = 0; index < extractor.getTrackCount(); ++index) {
            if (VERBOSE) {
                Log.d(TAG, "format for track " + index + " is " + getMimeTypeFor(extractor.getTrackFormat(index)));
            }
            if (isAudioFormat(extractor.getTrackFormat(index))) {
                extractor.selectTrack(index);
                return index;
            }
        }
        return -1;
    }

    ByteBuffer[] videoDecoderInputBuffers = null;
    ByteBuffer[] videoDecoderOutputBuffers = null;
    ByteBuffer[] videoEncoderOutputBuffers = null;
    MediaCodec.BufferInfo videoDecoderOutputBufferInfo = null;
    MediaCodec.BufferInfo videoEncoderOutputBufferInfo = null;

    ByteBuffer[] audioDecoderInputBuffers = null;
    ByteBuffer[] audioDecoderOutputBuffers = null;
    ByteBuffer[] audioEncoderInputBuffers = null;
    ByteBuffer[] audioEncoderOutputBuffers = null;
    MediaCodec.BufferInfo audioDecoderOutputBufferInfo = null;
    MediaCodec.BufferInfo audioEncoderOutputBufferInfo = null;

    boolean videoExtractorDone = false;
    boolean videoDecoderDone = false;
    boolean audioExtractorDone = false;
    boolean audioDecoderDone = false;

    private long lastVideoDecoderFinalFrameTimestamp;
    private long temporaryVideoDecoderTimestamp;
    private long videoDecoderTimestampOffset = 0;

    private long lastAudioDecoderFinalFrameTimestamp;
    private long temporaryAudioDecoderTimestamp;
    private long audioDecoderTimestampOffset = 0;

    int pendingAudioDecoderOutputBufferIndex = -1;

    private int currentSilenceFrameCount = 0;

    private void feedSilenceToEncoder(int frameCount)
    {
        while (mCopyAudio) {
            if (VERBOSE) {
                Log.d(TAG,
                        "audio decoder: attempting to add silence: "+frameCount+" "+(silenceFrameCount - 1)+" "+(frameCount >= (silenceFrameCount - 1)));
            }
            int encoderInputBufferIndex = audioEncoder.dequeueInputBuffer(TIMEOUT_USEC);
            if (encoderInputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                if (VERBOSE)
                    Log.d(TAG, "no audio encoder input buffer");
                break;
            }
            if (VERBOSE) {
                Log.d(TAG, "audio encoder: returned input buffer: "
                        + encoderInputBufferIndex);
            }
            ByteBuffer encoderInputBuffer = audioEncoderInputBuffers[encoderInputBufferIndex];
            int size = 4096;
            long presentationTime = audioDecoderTimestampOffset + frameCount * projectedTimestamp;
            if (VERBOSE) {
                Log.d(TAG, "audio decoder: processing pending buffer: "
                        + pendingAudioDecoderOutputBufferIndex);
            }
            if (VERBOSE) {
                Log.d(TAG, "audio decoder: pending buffer of size " + size);
                Log.d(TAG, "audio decoder: pending buffer for time "
                        + presentationTime);
            }
            if (size >= 0) {
                byte[] array = new byte[4096];
                Arrays.fill(array, (byte)0);
                ByteBuffer decoderOutputBuffer = ByteBuffer.wrap(array);

                decoderOutputBuffer.position(0);
                decoderOutputBuffer.limit(size);
                encoderInputBuffer.position(0);

                encoderInputBuffer.put(decoderOutputBuffer);
                if(audioUriIndex < mUriList.size()-1) {
                    if(frameCount != silenceFrameCount - 1) {
                        audioEncoder.queueInputBuffer(encoderInputBufferIndex, 0, size, presentationTime, 0);
                    }
                }
                else
                {
                    audioEncoder.queueInputBuffer(encoderInputBufferIndex, 0, size, presentationTime, (frameCount < (silenceFrameCount - 1)) ? 0 : MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                }
            }
            if (frameCount >= silenceFrameCount - 1) {
                if (VERBOSE)
                    Log.d(TAG, "audio silence provider: EOS");
                if (audioUriIndex == mUriList.size() - 1) {
                    audioDecoderDone = true;
                } else {
                    if (this.audioExtractor != null) {
                        this.audioExtractor.release();
                        this.audioExtractor = null;
                    }
                    if (this.audioDecoder != null) {
                        this.audioDecoder.stop();
                        this.audioDecoder.release();
                        this.audioDecoder = null;
                    }
                    try {
                        lastAudioDecoderFinalFrameTimestamp += (silenceFrameCount - 1) * projectedTimestamp + 33333;
                        temporaryAudioDecoderTimestamp = 0;
                        audioDecoderTimestampOffset = lastAudioDecoderFinalFrameTimestamp;

                        ++audioUriIndex;
                        initializeAudioProcessLoop(audioUriIndex);
                        if(this.audioDecoder != null)
                        {
                            audioDecoderInputBuffers = this.audioDecoder.getInputBuffers();
                            audioDecoderOutputBuffers = this.audioDecoder.getOutputBuffers();
                            audioDecoderOutputBufferInfo = new MediaCodec.BufferInfo();
                        }
                        audioEncoder.flush();

                        silenceFrameCount = calcNumOfAudioFrames(audioUriIndex);
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                }
                currentSilenceFrameCount = 0;
            }
            break;
        }
    }

    private void doExtractDecodeEditEncodeMux(MediaExtractor videoExtractor,
                                              MediaExtractor audioExtractor, MediaCodec videoDecoder,
                                              MediaCodec videoEncoder, MediaCodec audioDecoder,
                                              MediaCodec audioEncoder, MediaMuxer muxer,
                                              InputSurface inputSurface, OutputSurface outputSurface,
                                              float viewportX, float viewportY) {
        int gifFrameCount = 0;

        if (mCopyVideo) {
            videoDecoderInputBuffers = videoDecoder.getInputBuffers();
            videoDecoderOutputBuffers = videoDecoder.getOutputBuffers();
            videoDecoderOutputBufferInfo = new MediaCodec.BufferInfo();
            videoEncoderOutputBuffers = videoEncoder.getOutputBuffers();
            videoEncoderOutputBufferInfo = new MediaCodec.BufferInfo();
        }
        if (mCopyAudio) {
            if(videoHasSound) {
                audioDecoderInputBuffers = audioDecoder.getInputBuffers();
                audioDecoderOutputBuffers = audioDecoder.getOutputBuffers();
                audioDecoderOutputBufferInfo = new MediaCodec.BufferInfo();
            }
            audioEncoderInputBuffers = audioEncoder.getInputBuffers();
            audioEncoderOutputBuffers = audioEncoder.getOutputBuffers();
            audioEncoderOutputBufferInfo = new MediaCodec.BufferInfo();
        }
        MediaFormat decoderOutputVideoFormat = null;
        MediaFormat decoderOutputAudioFormat = null;
        MediaFormat encoderOutputVideoFormat = null;
        MediaFormat encoderOutputAudioFormat = null;
        int outputVideoTrack = -1;
        int outputAudioTrack = -1;
        videoExtractorDone = false;
        videoDecoderDone = false;
        boolean videoEncoderDone = false;
        audioExtractorDone = false;
        audioDecoderDone = false;
        boolean audioEncoderDone = false;

        boolean muxing = false;

        int videoExtractedFrameCount = 0;
        int videoDecodedFrameCount = 0;
        int videoEncodedFrameCount = 0;

        int audioExtractedFrameCount = 0;
        int audioDecodedFrameCount = 0;
        int audioEncodedFrameCount = 0;

        while ((mCopyVideo && !videoEncoderDone)
                || (mCopyAudio && !audioEncoderDone)) {
            if (VERBOSE) {
                Log.d(TAG, String.format("loop: "

                                + "V(%b){" + "extracted:%d(doneMenuClick:%b) " + "decoded:%d(doneMenuClick:%b) "
                                + "encoded:%d(doneMenuClick:%b)} "

                                + "A(%b){" + "extracted:%d(doneMenuClick:%b) "
                                + "decoded:%d(doneMenuClick:%b) " + "encoded:%d(doneMenuClick:%b) "
                                + "pending:%d} "

                                + "muxing:%b(V:%d,A:%d)",

                        mCopyVideo, videoExtractedFrameCount, videoExtractorDone,
                        videoDecodedFrameCount, videoDecoderDone,
                        videoEncodedFrameCount, videoEncoderDone,

                        mCopyAudio, audioExtractedFrameCount,
                        audioExtractorDone, audioDecodedFrameCount,
                        audioDecoderDone, audioEncodedFrameCount,
                        audioEncoderDone, pendingAudioDecoderOutputBufferIndex,
                        muxing, outputVideoTrack, outputAudioTrack));
            }

            while (mCopyVideo && !videoExtractorDone
                    && (encoderOutputVideoFormat == null || muxing)) {
                int decoderInputBufferIndex = this.videoDecoder.dequeueInputBuffer(TIMEOUT_USEC);
                if (decoderInputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    if (VERBOSE)
                        Log.d(TAG, "no video decoder input buffer");
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "video decoder: returned input buffer: "
                            + decoderInputBufferIndex);
                }
                ByteBuffer decoderInputBuffer = videoDecoderInputBuffers[decoderInputBufferIndex];
                int size = this.videoExtractor.readSampleData(decoderInputBuffer, 0);
                long presentationTime = this.videoExtractor.getSampleTime();
                if (VERBOSE) {
                    Log.d(TAG, "video extractor: returned buffer of size " + size);
                    Log.d(TAG, "video extractor: returned buffer for time " + presentationTime);
                }
                if (size >= 0) {
                    if(presentationTime > temporaryVideoDecoderTimestamp){temporaryVideoDecoderTimestamp = presentationTime;}
                    this.videoDecoder.queueInputBuffer(decoderInputBufferIndex, 0, size, presentationTime, this.videoExtractor.getSampleFlags());
                }
                videoExtractorDone = !this.videoExtractor.advance();
                if (videoExtractorDone) {
                    if (VERBOSE)
                        Log.d(TAG, "video extractor: EOS");
                    lastVideoDecoderFinalFrameTimestamp += temporaryVideoDecoderTimestamp + 33333;
                    temporaryVideoDecoderTimestamp = 0;
                    
                    this.videoDecoder.queueInputBuffer(decoderInputBufferIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);

                }
                videoExtractedFrameCount++;
                break;
            }

            while (mCopyAudio && !audioExtractorDone && (encoderOutputAudioFormat == null || muxing) && videoHasSound) {
                int decoderInputBufferIndex = this.audioDecoder.dequeueInputBuffer(TIMEOUT_USEC);
                if (decoderInputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    if (VERBOSE)
                        Log.d(TAG, "no audio decoder input buffer");
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "audio decoder: returned input buffer: "
                            + decoderInputBufferIndex);
                }
                ByteBuffer decoderInputBuffer = audioDecoderInputBuffers[decoderInputBufferIndex];
                int size = this.audioExtractor.readSampleData(decoderInputBuffer, 0);
                long presentationTime = this.audioExtractor.getSampleTime();
                if (VERBOSE) {
                    Log.d(TAG, "audio extractor: returned buffer of size "
                            + size);
                    Log.d(TAG, "audio extractor: returned buffer for time "
                            + presentationTime);
                }
                if (size >= 0) {
                    if(presentationTime > temporaryAudioDecoderTimestamp){temporaryAudioDecoderTimestamp = presentationTime;}
                    this.audioDecoder.queueInputBuffer(decoderInputBufferIndex, 0,
                            size, presentationTime,
                            this.audioExtractor.getSampleFlags());
                }
                audioExtractorDone = !this.audioExtractor.advance();
                if (audioExtractorDone) {
                    if (VERBOSE)
                        Log.d(TAG, "audio extractor: EOS");
                    lastAudioDecoderFinalFrameTimestamp += temporaryAudioDecoderTimestamp + 33333;
                    temporaryAudioDecoderTimestamp = 0;

                    this.audioDecoder.queueInputBuffer(decoderInputBufferIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                }
                audioExtractedFrameCount++;
                break;
            }

            while (mCopyVideo && !videoDecoderDone
                    && (encoderOutputVideoFormat == null || muxing)) {
                int decoderOutputBufferIndex = this.videoDecoder.dequeueOutputBuffer(videoDecoderOutputBufferInfo, TIMEOUT_USEC);
                videoDecoderOutputBufferInfo.presentationTimeUs += videoDecoderTimestampOffset;
                Log.i(TAG, "frame timestamp = "+videoDecoderOutputBufferInfo.presentationTimeUs+" size = "+videoDecoderOutputBufferInfo.size);
                if (decoderOutputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    if (VERBOSE)
                        Log.d(TAG, "no video decoder output buffer");
                    break;
                }
                if (decoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                    if (VERBOSE)
                        Log.d(TAG, "video decoder: output buffers changed");
                    videoDecoderOutputBuffers = this.videoDecoder.getOutputBuffers();
                    break;
                }
                if (decoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                    decoderOutputVideoFormat = this.videoDecoder.getOutputFormat();
                    Log.i("decoder_vid_out_format", decoderOutputVideoFormat+"");
                    if (VERBOSE) {
                        Log.d(TAG, "video decoder: output format changed: "
                                + decoderOutputVideoFormat);
                    }
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "video decoder: returned output buffer: "
                            + decoderOutputBufferIndex);
                    Log.d(TAG, "video decoder: returned buffer of size "
                            + videoDecoderOutputBufferInfo.size);
                    Log.d(TAG, "video decoder: returned buffer for time "
                            + videoDecoderOutputBufferInfo.presentationTimeUs);
                }
                ByteBuffer decoderOutputBuffer = videoDecoderOutputBuffers[decoderOutputBufferIndex];
                if ((videoDecoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    if (VERBOSE)
                        Log.d(TAG, "video decoder: codec config buffer");
                    this.videoDecoder.releaseOutputBuffer(decoderOutputBufferIndex,
                            false);
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "video decoder: returned buffer for time "
                            + videoDecoderOutputBufferInfo.presentationTimeUs);
                }
                boolean render = videoDecoderOutputBufferInfo.size != 0;
                this.videoDecoder.releaseOutputBuffer(decoderOutputBufferIndex,
                        render);
                if (render) {
                    if (VERBOSE)
                        Log.d(TAG, "output surface: await new image");
                    this.outputSurface.awaitNewImage();
                    if (VERBOSE)
                        Log.d(TAG, "output surface: draw image");

                    this.outputSurface.drawImage();

                    this.inputSurface.setPresentationTime(videoDecoderOutputBufferInfo.presentationTimeUs * 1000);
                    if (VERBOSE)
                        Log.d(TAG, "input surface: swap buffers");
                    this.inputSurface.swapBuffers();
                    if (VERBOSE)
                        Log.d(TAG, "video encoder: notified of new frame");
                }
                videoDecodedFrameCount++;
                if ((videoDecoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    if (VERBOSE)
                        Log.d(TAG, "video decoder: EOS");
                    if(videoUriIndex == mUriList.size()-1)
                    {
                        lastVideoDecoderFinalFrameTimestamp += temporaryVideoDecoderTimestamp + 33333;
                        temporaryVideoDecoderTimestamp = 0;
                        videoDecoderTimestampOffset = lastVideoDecoderFinalFrameTimestamp;

                        videoDecoderDone = true;
                        videoEncoder.signalEndOfInputStream();
                    }
                    else
                    {
                        if(this.videoExtractor != null)
                        {
                            this.videoExtractor.release();
                            this.videoExtractor = null;
                        }
                        if(this.videoDecoder != null){
                            this.videoDecoder.stop();
                            this.videoDecoder.release();
                            this.videoDecoder = null;
                        }
                        try{
                            lastVideoDecoderFinalFrameTimestamp += temporaryVideoDecoderTimestamp + 33333;
                            temporaryVideoDecoderTimestamp = 0;
                            videoDecoderTimestampOffset = lastVideoDecoderFinalFrameTimestamp;

                            ++videoUriIndex;

                            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                            mmr.setDataSource(mUriList.get(videoUriIndex));
                            videoWidht = Integer.valueOf(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
                            videoHeight = Integer.valueOf(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
                            orientation = Integer.valueOf(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION));
                            Log.i("vid_rotation", videoWidht+" "+videoHeight+" "+orientation);

                            initializeVideoProcessLoop(videoUriIndex);
                            videoEncoder.flush();
                        }
                        catch(IOException ioe){ioe.printStackTrace();}
                    }
                }
                break;
            }

            if(videoHasSound)
            {
                while (mCopyAudio && !audioDecoderDone && pendingAudioDecoderOutputBufferIndex == -1 && (encoderOutputAudioFormat == null || muxing)) {
                    int decoderOutputBufferIndex = this.audioDecoder.dequeueOutputBuffer(audioDecoderOutputBufferInfo, TIMEOUT_USEC);
                    audioDecoderOutputBufferInfo.presentationTimeUs += audioDecoderTimestampOffset;
                    if (decoderOutputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                        if (VERBOSE)
                            Log.d(TAG, "no audio decoder output buffer");
                        break;
                    }
                    if (decoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                        if (VERBOSE)
                            Log.d(TAG, "audio decoder: output buffers changed");
                        audioDecoderOutputBuffers = this.audioDecoder.getOutputBuffers();
                        break;
                    }
                    if (decoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                        decoderOutputAudioFormat = this.audioDecoder.getOutputFormat();
                        decoderOutputChannelNum = decoderOutputAudioFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
                        decoderOutputAudioSampleRate = decoderOutputAudioFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE);
                        if (VERBOSE) {
                            Log.d(TAG, "audio decoder: output format changed: " + decoderOutputAudioFormat);
                        }
                        break;
                    }
                    if (VERBOSE) {
                        Log.d(TAG, "audio decoder: returned output buffer: "
                                + decoderOutputBufferIndex);
                    }
                    if (VERBOSE) {
                        Log.d(TAG, "audio decoder: returned buffer of size "
                                + audioDecoderOutputBufferInfo.size);
                    }

                    if ((audioDecoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                        if (VERBOSE)
                            Log.d(TAG, "audio decoder: codec config buffer");
                        this.audioDecoder.releaseOutputBuffer(decoderOutputBufferIndex,
                                false);
                        break;
                    }
                    if (VERBOSE) {
                        Log.d(TAG, "audio decoder: returned buffer for time "
                                + audioDecoderOutputBufferInfo.presentationTimeUs);
                    }
                    if (VERBOSE) {
                        Log.d(TAG, "audio decoder: output buffer is now pending: "
                                + pendingAudioDecoderOutputBufferIndex);
                    }
                    pendingAudioDecoderOutputBufferIndex = decoderOutputBufferIndex;
                    audioDecodedFrameCount++;
                    break;
                }

                while (mCopyAudio && pendingAudioDecoderOutputBufferIndex != -1) {
                    if (VERBOSE) {
                        Log.d(TAG,
                                "audio decoder: attempting to process pending buffer: " + pendingAudioDecoderOutputBufferIndex);
                    }
                    int encoderInputBufferIndex = audioEncoder.dequeueInputBuffer(TIMEOUT_USEC);
                    if (encoderInputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                        if (VERBOSE)
                            Log.d(TAG, "no audio encoder input buffer");
                        break;
                    }
                    if (VERBOSE) {
                        Log.d(TAG, "audio encoder: returned input buffer: "
                                + encoderInputBufferIndex);
                    }
                    ByteBuffer encoderInputBuffer = audioEncoderInputBuffers[encoderInputBufferIndex];
                    int size = audioDecoderOutputBufferInfo.size;
                    long presentationTime = audioDecoderOutputBufferInfo.presentationTimeUs;
                    if (VERBOSE) {
                        Log.d(TAG, "audio decoder: processing pending buffer: "
                                + pendingAudioDecoderOutputBufferIndex);
                    }
                    if (VERBOSE) {
                        Log.d(TAG, "audio decoder: pending buffer of size " + size);
                        Log.d(TAG, "audio decoder: pending buffer for time "
                                + presentationTime);
                    }
                    if (size >= 0) {
                        ByteBuffer decoderOutputBuffer = audioDecoderOutputBuffers[pendingAudioDecoderOutputBufferIndex]
                                .duplicate();

                        byte[] testBufferContents = new byte[size]; //For copying the bytebuffer's contents into.

                        float samplingFactor = (float) Math.ceil((float) decoderOutputChannelNum / OUTPUT_AUDIO_CHANNEL_COUNT) * (float) Math.round((float) decoderOutputAudioSampleRate / OUTPUT_AUDIO_SAMPLE_RATE);

                        int bufferSize = size / (int) samplingFactor;
                        //The initial buffer size

                        if ((audioDecoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) == 0) {

                            if (decoderOutputBuffer.remaining() < size) {
                                for (int i = decoderOutputBuffer.remaining(); i < size; i++) {
                                    testBufferContents[i] = 0;  // pad with extra 0s to make a full frame.
                                }
                                decoderOutputBuffer.get(testBufferContents, 0, decoderOutputBuffer.remaining());
                            } else {
                                decoderOutputBuffer.get(testBufferContents, 0, size);
                            }

                            if (decoderOutputAudioSampleRate % 22050 != 0 && OUTPUT_AUDIO_SAMPLE_RATE % 22050 == 0) //in case the current decoder output's sample rate is 48000 and the first one's 44100
                            {
                                testBufferContents = minorDownsamplingFrom48kTo44k(testBufferContents);
                                bufferSize = testBufferContents.length / (int) samplingFactor;
                                if (bufferSize % 2 != 0) {
                                    bufferSize -= (bufferSize % 2);
                                } //NO ODD VALUES PLOX
                            } else if (decoderOutputAudioSampleRate % 22050 == 0 && OUTPUT_AUDIO_SAMPLE_RATE % 22050 != 0) //in case the current decoder output's sample rate is 44100 and the first one's 48000
                            {
                                testBufferContents = minorUpsamplingFrom44kTo48k(testBufferContents);
                                bufferSize = testBufferContents.length / (int) samplingFactor;
                                if (bufferSize % 2 != 0) {
                                    bufferSize -= (bufferSize % 2);
                                } //NO ODD VALUES PLOX
                            } else {

                            }

                            if (((int) samplingFactor) > 1) {
                                byte[] finalByteBufferContent = new byte[bufferSize];
                                //Kids, 7524 / 2 != 4096

                                for (int i = 0; i < bufferSize; i += 2) {
                                    if ((i + 1) * ((int) samplingFactor) > testBufferContents.length) {
                                        finalByteBufferContent[i] = 0;
                                        finalByteBufferContent[i + 1] = 0;
                                    } else {
                                        finalByteBufferContent[i] = testBufferContents[i * ((int) samplingFactor)];
                                        finalByteBufferContent[i + 1] = testBufferContents[i * ((int) samplingFactor) + 1];
                                    }
                                }

                                bufferSize = finalByteBufferContent.length;
                                decoderOutputBuffer = ByteBuffer.wrap(finalByteBufferContent);
                            }
                            else
                            {
                                //PLEASE DO NOT FORGET TO WRAP THE BYTE ARRAY INTO A BYTE BUFFER LOL
                                bufferSize = testBufferContents.length;
                                decoderOutputBuffer = ByteBuffer.wrap(testBufferContents);
                            }
                        }

                        decoderOutputBuffer.position(audioDecoderOutputBufferInfo.offset);
                        decoderOutputBuffer.limit(audioDecoderOutputBufferInfo.offset + bufferSize);
                        encoderInputBuffer.position(0);

                        encoderInputBuffer.put(decoderOutputBuffer);

                        if (audioUriIndex < mUriList.size() - 1) {
                            if ((audioDecoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) == 0) {
                                audioEncoder.queueInputBuffer(encoderInputBufferIndex, 0, bufferSize, presentationTime, audioDecoderOutputBufferInfo.flags);
                            }
                        } else {
                            audioEncoder.queueInputBuffer(encoderInputBufferIndex, 0, bufferSize, presentationTime, audioDecoderOutputBufferInfo.flags);
                        }
                    }
                    this.audioDecoder.releaseOutputBuffer(pendingAudioDecoderOutputBufferIndex, false);
                    pendingAudioDecoderOutputBufferIndex = -1;

                    if ((audioDecoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                        if (VERBOSE)
                            Log.d(TAG, "audio decoder: EOS");
                        if (audioUriIndex == mUriList.size() - 1) {
                            lastAudioDecoderFinalFrameTimestamp += temporaryAudioDecoderTimestamp + 33333;
                            temporaryAudioDecoderTimestamp = 0;
                            audioDecoderTimestampOffset = lastAudioDecoderFinalFrameTimestamp;
                            audioDecoderDone = true;
                        } else {
                            if (this.audioExtractor != null) {
                                this.audioExtractor.release();
                                this.audioExtractor = null;
                            }
                            if (this.audioDecoder != null) {
                                this.audioDecoder.stop();
                                this.audioDecoder.release();
                                this.audioDecoder = null;
                            }
                            try {
                                lastAudioDecoderFinalFrameTimestamp += temporaryAudioDecoderTimestamp + 33333;
                                temporaryAudioDecoderTimestamp = 0;
                                audioDecoderTimestampOffset = lastAudioDecoderFinalFrameTimestamp;

                                ++audioUriIndex;
                                initializeAudioProcessLoop(audioUriIndex);

                                //The encoder has to be flushed at minimum every four videos. I chose to flush at the end of each decoder cycle / every video.
                                //Otherwise the leftover data will clog the encoder (encoder MediaCodec.dequeueInputBuffer won't return all buffer indices)
                                audioEncoder.flush();

                                silenceFrameCount = calcNumOfAudioFrames(audioUriIndex);
                            } catch (IOException ioe) {
                                ioe.printStackTrace();
                            }
                        }
                    }
                    break;
                }
            }
            else
            {
                feedSilenceToEncoder(currentSilenceFrameCount);
                currentSilenceFrameCount++;
            }

            while (mCopyVideo && !videoEncoderDone
                    && (encoderOutputVideoFormat == null || muxing)) {

                int encoderOutputBufferIndex = videoEncoder.dequeueOutputBuffer(videoEncoderOutputBufferInfo, TIMEOUT_USEC);

                if (encoderOutputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    if (VERBOSE)
                        Log.d(TAG, "no video encoder output buffer");
                    break;
                }
                if (encoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                    if (VERBOSE)
                        Log.d(TAG, "video encoder: output buffers changed");
                    videoEncoderOutputBuffers = videoEncoder.getOutputBuffers();
                    break;
                }
                if (encoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                    if (VERBOSE)
                        Log.d(TAG, "video encoder: output format changed");
                    if (outputVideoTrack >= 0) {
                        Log.d(TAG,
                                "video encoder changed its output format again?");
                    }
                    encoderOutputVideoFormat = videoEncoder.getOutputFormat();
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "video encoder: returned output buffer: "
                            + encoderOutputBufferIndex);
                    Log.d(TAG, "video encoder: returned buffer of size "
                            + videoEncoderOutputBufferInfo.size);
                }
                ByteBuffer encoderOutputBuffer = videoEncoderOutputBuffers[encoderOutputBufferIndex];
                if ((videoEncoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    if (VERBOSE)
                        Log.d(TAG, "video encoder: codec config buffer");
                    videoEncoder.releaseOutputBuffer(encoderOutputBufferIndex,
                            false);
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "video encoder: returned buffer for time "
                            + videoEncoderOutputBufferInfo.presentationTimeUs);
                }

                if (videoEncoderOutputBufferInfo.size != 0) {
                    muxer.writeSampleData(outputVideoTrack,
                            encoderOutputBuffer, videoEncoderOutputBufferInfo);
                }
                if ((videoEncoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    if (VERBOSE)
                        Log.d(TAG, "video encoder: EOS");
                    videoEncoderDone = true;
                } else {
                    videoProgress = (int)(((float)videoEncoderOutputBufferInfo.presentationTimeUs / totalTime) * 100);
                }
                videoEncoder.releaseOutputBuffer(encoderOutputBufferIndex,
                        false);
                videoEncodedFrameCount++;
                break;
            }

            while (mCopyAudio && !audioEncoderDone && (encoderOutputAudioFormat == null || muxing)) {
                int encoderOutputBufferIndex = audioEncoder.dequeueOutputBuffer(audioEncoderOutputBufferInfo, TIMEOUT_USEC);
                //TODO: Needs emergency measures in case the encoder gets stalled on the fifth video
                if (encoderOutputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    if (VERBOSE)
                        Log.d(TAG, "no audio encoder output buffer");
                    break;
                }
                if (encoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                    if (VERBOSE)
                        Log.d(TAG, "audio encoder: output buffers changed");
                    audioEncoderOutputBuffers = audioEncoder.getOutputBuffers();
                    break;
                }
                if (encoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                    if (VERBOSE)
                        Log.d(TAG, "audio encoder: output format changed: "+audioEncoder.getOutputFormat());
                    if (outputAudioTrack >= 0) {
                        Log.d(TAG,
                                "audio encoder changed its output format again?");
                    }

                    encoderOutputAudioFormat = audioEncoder.getOutputFormat();
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "audio encoder: returned output buffer: "
                            + encoderOutputBufferIndex);
                    Log.d(TAG, "audio encoder: returned buffer of size "
                            + audioEncoderOutputBufferInfo.size);
                }
                ByteBuffer encoderOutputBuffer = audioEncoderOutputBuffers[encoderOutputBufferIndex];
                if ((audioEncoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    if (VERBOSE)
                        Log.d(TAG, "audio encoder: codec config buffer");
                    audioEncoder.releaseOutputBuffer(encoderOutputBufferIndex,
                            false);
                    break;
                }
                if (VERBOSE) {
                    Log.d(TAG, "audio encoder: returned buffer for time " + audioEncoderOutputBufferInfo.presentationTimeUs);
                }

                if (audioEncoderOutputBufferInfo.size != 0 && audioEncoderOutputBufferInfo.presentationTimeUs >= prevEncoderPresTime) {
                    muxer.writeSampleData(outputAudioTrack,
                            encoderOutputBuffer, audioEncoderOutputBufferInfo);
                    prevEncoderPresTime = audioEncoderOutputBufferInfo.presentationTimeUs;
                }
                Log.i("pres_times", audioEncoderOutputBufferInfo.presentationTimeUs+" "+prevEncoderPresTime);
                if ((audioEncoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    if (VERBOSE)
                        Log.d(TAG, "audio encoder: EOS");
                    audioEncoderDone = true;
                } else {
                    audioProgress = (int)(((float)audioEncoderOutputBufferInfo.presentationTimeUs / totalTime) * 100);
                }
                audioEncoder.releaseOutputBuffer(encoderOutputBufferIndex,
                        false);
                audioEncodedFrameCount++;
                break;
            }

            progIndicator.setProgress(mCopyAudio ? (videoProgress + audioProgress) / 2 : videoProgress);

            if (!muxing && (!mCopyAudio || encoderOutputAudioFormat != null)
                    && (!mCopyVideo || encoderOutputVideoFormat != null)) {
                if (mCopyVideo) {
                    Log.d(TAG, "muxer: adding video track.");
                    outputVideoTrack = muxer.addTrack(encoderOutputVideoFormat);
                }
                if (mCopyAudio) {
                    Log.d(TAG, "muxer: adding audio track.");
                    outputAudioTrack = muxer.addTrack(encoderOutputAudioFormat);
                }
                Log.d(TAG, "muxer: starting");
                muxer.start();
                muxing = true;
            }
        }
    }

    //Prototype function
    private byte[] minorDownsamplingFrom48kTo44k(byte[] origByteArray)
    {
        int origLength = origByteArray.length;
        int moddedLength = origLength * 147/160;

        if(moddedLength % 4 != 0){moddedLength -= (moddedLength % 4);} //Round DOWN just to be safe - safer to lose one sample than having too big an array

        byte[] resultByteArray = new byte[moddedLength];
        int arrayIndex = 0;

        //In the space of 11 samples in resultByteArray, copy 11 samples from the original
        //Then dump the 12th
        for(int i = 0; i < origLength; i+=48)
        {
            for(int j = i; j < (i+44 > origLength ? origLength : i + 44); j++)
            {
                if(arrayIndex < moddedLength) {
                    resultByteArray[arrayIndex] = origByteArray[j];
                    arrayIndex++;
                }
            }
        }

        //WARNING: This formula still introduces noise. Maybe some filtering is needed.

        return resultByteArray;
    }

    private byte[] minorUpsamplingFrom44kTo48k(byte[] origByteArray)
    {
        int origLength = origByteArray.length;
        int moddedLength = origLength * 160/147;

        if(moddedLength % 4 != 0){moddedLength += 2;} //Round DOWN just to be safe - safer to lose one sample than having too big an array and getting NOISE

        byte[] resultByteArray = new byte[moddedLength];

        //in the space of 12 samples in resultByteArray, copy 11 samples from the original
        //then, in the 12th, do linear interpolation between the 11th and 12th original samples.
        //Should I just stuff zeros instead, though?
        int iterationCount = 0;
        for(int i = 0; i < origLength; i+= 48)
        {
            for(int j = 0; j < (iterationCount*48 + 48 > origLength ? (origLength - iterationCount*48) : 48); j++)
            {
                resultByteArray[iterationCount * 52 + j] = origByteArray[iterationCount * 48 + j];
            }
            if(!(iterationCount*48 + 48 > origLength)) {
                resultByteArray[iterationCount * 52 + 48] = averageTwoBytes(origByteArray[iterationCount * 48 + 44], origByteArray[iterationCount * 48 + 48]);
                resultByteArray[iterationCount * 52 + 49] = averageTwoBytes(origByteArray[iterationCount * 48 + 45], origByteArray[iterationCount * 48 + 49]);
                resultByteArray[iterationCount * 52 + 50] = averageTwoBytes(origByteArray[iterationCount * 48 + 46], origByteArray[iterationCount * 48 + 50]);
                resultByteArray[iterationCount * 52 + 51] = averageTwoBytes(origByteArray[iterationCount * 48 + 47], origByteArray[iterationCount * 48 + 51]);
            }
            iterationCount++;
        }

        return resultByteArray;
    }

    //Might need more functions just in case I have to deal with videos that have audio tracks with 8000 Hz sample rate (who uses that though?)
    //Also, low-pass filtering?

    //This is my attempt at linear interpolation lol - though it's two bytes, not two samples [4 bytes each].
    private byte averageTwoBytes(byte byteOne, byte byteTwo)
    {
        return (byte)(((float)byteOne + (float) byteTwo)/2);
    }

    private static boolean isVideoFormat(MediaFormat format) {
        return getMimeTypeFor(format).startsWith("video/");
    }

    private static boolean isAudioFormat(MediaFormat format) {
        return getMimeTypeFor(format).startsWith("audio/");
    }

    private static String getMimeTypeFor(MediaFormat format) {
        return format.getString(MediaFormat.KEY_MIME);
    }

    private static MediaCodecInfo selectCodec(String mimeType) {
        int numCodecs = MediaCodecList.getCodecCount();
        for (int i = 0; i < numCodecs; i++) {
            MediaCodecInfo codecInfo = MediaCodecList.getCodecInfoAt(i);
            if (!codecInfo.isEncoder()) {
                continue;
            }
            String[] types = codecInfo.getSupportedTypes();
            for (int j = 0; j < types.length; j++) {
                if (types[j].equalsIgnoreCase(mimeType)) {
                    return codecInfo;
                }
            }
        }
        return null;
    }


    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }


}
