package com.abcode.nativevideo.codec;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.SurfaceTexture;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.Build;
import android.util.Log;

import com.abcode.nativevideo.utils.GLTextureHelper;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * Created by alfironi on 6/26/2015.
 */
class TextureRender {
    private static final String TAG = "TextureRender";

    private Context context;

    private static final int FLOAT_SIZE_BYTES = 4;
    private static final int TRIANGLE_VERTICES_DATA_STRIDE_BYTES = 5 * FLOAT_SIZE_BYTES;
    private static final int TRIANGLE_VERTICES_DATA_POS_OFFSET = 0;
    private static final int TRIANGLE_VERTICES_DATA_UV_OFFSET = 3;
    private final float[] mTriangleVerticesData = {
            // X, Y, Z, U, V
            -1.0f, -1.0f, 0, 0.f, 0.f,
            1.0f, -1.0f, 0, 1.f, 0.f,
            -1.0f, 1.0f, 0, 0.f, 1.f,
            1.0f, 1.0f, 0, 1.f, 1.f,};
    //overlay
    private final float[] mTriangleVerticesOverlayData = {
            // X, Y, Z, U, V
            -1.0f, -1.0f, 0, 0.f, 0.f,
            1.0f, -1.0f, 0, 1.f, 0.f,
            -1.0f, 1.0f, 0, 0.f, 1.f,
            1.0f, 1.0f, 0, 1.f, 1.f,
    };

    private float watermarkLeftCoord = (float)(720 - 144 - 20);
    private float watermarkLeftRatio = (watermarkLeftCoord / 720f)*2f - 1f;
    private float watermarkRightRatio = ((watermarkLeftCoord+144) / 720f)*2f - 1f;

    private float watermarkTopCoord = (float)(44 + 20);
    private float watermarkTopRatio = (watermarkTopCoord / 720f)*2f - 1f;
    private float watermarkBotRatio = ((watermarkTopCoord - 44) / 720f)*2f - 1f;

    private float[] parameterValues;

    private int filterInputTextureUniform2, filterInputTextureUniform3, filterInputTextureUniform4, filterInputTextureUniform5, filterInputTextureUniform6;
    private int filterSourceTexture2 = -1, filterSourceTexture3 = -1, filterSourceTexture4 = -1, filterSourceTexture5 = -1, filterSourceTexture6 = -1;

    private int uParamValue1Handle, uParamValue2Handle, uParamValue3Handle;

    private final float[] mTriangleVerticesWatermarkData = {
            // X, Y, Z, U, V
            watermarkLeftRatio, watermarkBotRatio, 0, 0.f, 1.f,
            watermarkRightRatio, watermarkBotRatio, 0, 1.f, 1.f,
            watermarkLeftRatio, watermarkTopRatio, 0, 0.f, 0.f,
            watermarkRightRatio, watermarkTopRatio, 0, 1.f, 0.f,
    };

    private FloatBuffer mTriangleVertices;
    //overlay

    private static final String VERTEX_SHADER =
            "uniform mat4 uMVPMatrix;\n"
                    + "uniform mat4 uSTMatrix;\n"

                    + "attribute vec4 vPosition;\n"
                    + "attribute vec4 vTexCoordinate;\n"

                    + "varying vec2 v_TexCoordinate;\n"

                    + "void main() {\n"
                    + "  gl_Position = uMVPMatrix * vPosition;\n"
                    + "  v_TexCoordinate = (uSTMatrix * vTexCoordinate).xy;\n"
                    + "}\n";

    private static final String FRAGMENT_SHADER = "#extension GL_OES_EGL_image_external : require\n"
            + "precision mediump float;\n"
            + "varying vec2 v_TexCoordinate;\n"
            + "uniform samplerExternalOES u_Texture;\n"
            + "void main() {\n"
            + "  gl_FragColor = texture2D(u_Texture, v_TexCoordinate);\n"
            + "}\n";

    private static final String WATERMARK_VERTEX_SHADER =
            "uniform mat4 uMVPMatrix;\n"
                    + "attribute vec4 vPosition;\n"
                    + "attribute vec4 vTexCoordinate;\n"
                    + "varying vec2 v_TexCoordinate;\n"
                    + "void main() {\n"
                    + "  gl_Position = vPosition;\n"
                    + "  v_TexCoordinate = vTexCoordinate.xy;\n"
                    + "}\n";

    private static final String TEX2D_FRAGMENT_SHADER =
            "#extension GL_OES_EGL_image_external : require\n" +
                    "precision mediump float;\n" +
                    "varying vec2 v_TexCoordinate;\n" +
                    "uniform sampler2D sTexture;\n" +
                    "void main() {\n" +
                    "  gl_FragColor = texture2D(sTexture, v_TexCoordinate);\n" +
                    "}\n";

    private float[] mMVPMatrix = new float[16];
    private float[] mSTMatrix = new float[16];

    private int mProgram;
    private int mTextureID = -12345;
    private int muMVPMatrixHandle;
    private int muSTMatrixHandle;
    private int mvPositionHandle;
    private int maTextureHandle;

    int[] textures = new int[2];

    public TextureRender(Context context) {
        this.context = context;

        mTriangleVertices = ByteBuffer
                .allocateDirect(mTriangleVerticesData.length * FLOAT_SIZE_BYTES)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTriangleVertices.put(mTriangleVerticesData).position(0);

        GLES20.glGenTextures(2, textures, 0);
    }

    public int getTextureId() {
        return mTextureID;
    }

    public void preloadFilterAssets(int... resources)
    {
        textureCleanup();
        if(resources.length == 0)
        {
            return;
        }
        else
        {
            if (resources.length > 0) {
                Bitmap b1 = BitmapFactory.decodeResource(context.getResources(), resources[0]);
                filterSourceTexture2 = GLTextureHelper.loadTexture(b1);
                b1.recycle();
                b1 = null;
            }

            if (resources.length > 1) {
                Bitmap b2 = BitmapFactory.decodeResource(context.getResources(), resources[1]);
                filterSourceTexture3 = GLTextureHelper.loadTexture(b2);
                b2.recycle();
                b2 = null;
            }

            if (resources.length > 2) {
                Bitmap b3 = BitmapFactory.decodeResource(context.getResources(), resources[2]);
                filterSourceTexture4 = GLTextureHelper.loadTexture(b3);
                b3.recycle();
                b3 = null;
            }

            if (resources.length > 3) {
                Bitmap b4 = BitmapFactory.decodeResource(context.getResources(), resources[3]);
                filterSourceTexture5 = GLTextureHelper.loadTexture(b4);
                b4.recycle();
                b4 = null;
            }

            if (resources.length > 4) {
                Bitmap b5 = BitmapFactory.decodeResource(context.getResources(), resources[4]);
                filterSourceTexture6 = GLTextureHelper.loadTexture(b5);
                b5.recycle();
                b5 = null;
            }
        }
    }

    public void preloadValues(float... values)
    {
        parameterValues = values;
    }

    public void onPreDrawFrame()
    {
        if (filterSourceTexture2 != -1 && filterInputTextureUniform2 != -1) {
            GLES20.glActiveTexture(GLES20.GL_TEXTURE2);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, filterSourceTexture2);
            GLES20.glUniform1i(filterInputTextureUniform2, 2);
        }

        if (filterSourceTexture3 != -1 && filterInputTextureUniform3 != -1) {
            GLES20.glActiveTexture(GLES20.GL_TEXTURE3);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, filterSourceTexture3);
            GLES20.glUniform1i(filterInputTextureUniform3, 3);
        }

        if (filterSourceTexture4 != -1 && filterInputTextureUniform4 != -1) {
            GLES20.glActiveTexture(GLES20.GL_TEXTURE4);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, filterSourceTexture4);
            GLES20.glUniform1i(filterInputTextureUniform4, 4);
        }

        if (filterSourceTexture5 != -1 && filterInputTextureUniform5 != -1) {
            GLES20.glActiveTexture(GLES20.GL_TEXTURE5);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, filterSourceTexture5);
            GLES20.glUniform1i(filterInputTextureUniform5, 5);
        }

        if (filterSourceTexture6 != -1 && filterInputTextureUniform6 != -1) {
            GLES20.glActiveTexture(GLES20.GL_TEXTURE6);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, filterSourceTexture6);
            GLES20.glUniform1i(filterInputTextureUniform6, 6);
        }

        /*
        if (uParamValue1Handle != -1)
        {
            GLES20.glUniform1f(uParamValue1Handle, parameterValues[0]);
        }
        if (uParamValue2Handle != -1)
        {
            GLES20.glUniform1f(uParamValue2Handle, parameterValues[1]);
        }
        if (uParamValue3Handle != -1)
        {
            GLES20.glUniform1f(uParamValue3Handle, parameterValues[2]);
        }
        */
    }

    public void drawFrame(SurfaceTexture st) {
        checkGlError("onDrawFrame start");

        st.getTransformMatrix(mSTMatrix);
        //TODO: parse individual components of transformMatrix maybe, or just ignore it altogether and use the recording hints

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);

        checkGlError("glChangeViewport");

        filterInputTextureUniform2 = GLES20.glGetUniformLocation(mProgram, "inputImageTexture2");
        filterInputTextureUniform3 = GLES20.glGetUniformLocation(mProgram, "inputImageTexture3");
        filterInputTextureUniform4 = GLES20.glGetUniformLocation(mProgram, "inputImageTexture4");
        filterInputTextureUniform5 = GLES20.glGetUniformLocation(mProgram, "inputImageTexture5");
        filterInputTextureUniform6 = GLES20.glGetUniformLocation(mProgram, "inputImageTexture6");

        int mainTextureLoc = GLES20.glGetUniformLocation(mProgram, "u_Texture");
        checkGlError("glGetUniform");

        GLES20.glUseProgram(mProgram);
        checkGlError("glUseProgram");

        GLES20.glUniform1i(mainTextureLoc, 0);
        checkGlError("glSetUniform");

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mTextureID);
        checkGlError("glBindTextureMain");

        onPreDrawFrame();
        checkGlError("glPreDraw");

        mTriangleVertices.position(TRIANGLE_VERTICES_DATA_POS_OFFSET);
        GLES20.glVertexAttribPointer(mvPositionHandle, 3, GLES20.GL_FLOAT,
                false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, mTriangleVertices);
        checkGlError("glVertexAttribPointer mvPosition");
        GLES20.glEnableVertexAttribArray(mvPositionHandle);
        checkGlError("glEnableVertexAttribArray mvPositionHandle");

        mTriangleVertices.position(TRIANGLE_VERTICES_DATA_UV_OFFSET);
        GLES20.glVertexAttribPointer(maTextureHandle, 2, GLES20.GL_FLOAT,
                false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, mTriangleVertices);
        checkGlError("glVertexAttribPointer maTextureHandle");
        GLES20.glEnableVertexAttribArray(maTextureHandle);
        checkGlError("glEnableVertexAttribArray maTextureHandle");

        Matrix.setIdentityM(mMVPMatrix, 0);
        GLES20.glUniformMatrix4fv(muMVPMatrixHandle, 1, false, mMVPMatrix, 0);
        checkGlError("glViewportHandle");
        GLES20.glUniformMatrix4fv(muSTMatrixHandle, 1, false, mSTMatrix, 0);
        checkGlError("glTextureHandle");

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        checkGlError("glDrawArrays");
        //GLES20.glFinish();
    }

    public void surfaceCreated() {
        mProgram = createProgram(VERTEX_SHADER, FRAGMENT_SHADER);
        if (mProgram == 0) {
            throw new RuntimeException("failed creating program");
        }

        mvPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
        checkGlError("glGetAttribLocation vPosition");
        if (mvPositionHandle == -1) {
            throw new RuntimeException(
                    "Could not get attrib location for vPosition");
        }
        maTextureHandle = GLES20.glGetAttribLocation(mProgram, "vTexCoordinate");
        checkGlError("glGetAttribLocation vTexCoordinate");
        if (maTextureHandle == -1) {
            throw new RuntimeException(
                    "Could not get attrib location for vTexCoordinate");
        }

        muMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        checkGlError("glGetUniformLocation uMVPMatrix");
        if (muMVPMatrixHandle == -1) {
            throw new RuntimeException(
                    "Could not get attrib location for uMVPMatrix");
        }

        muSTMatrixHandle = GLES20.glGetUniformLocation(mProgram,
                "uSTMatrix");
        Log.i("muSTMatrixHandle", "loc = "+muSTMatrixHandle);
        checkGlError("glGetUniformLocation uSTMatrix");
        if (muSTMatrixHandle == -1) {
            throw new RuntimeException(
                    "Could not get attrib location for uSTMatrix");
        }

//        int[] textures = new int[1];
        GLES20.glGenTextures(2, textures, 0);

        mTextureID = textures[0];
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mTextureID);
        checkGlError("glBindTexture mTextureID");

        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
                GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
                GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
                GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
                GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        checkGlError("glTexParameter");
    }

    public void changeFragmentShader(String fragmentShader) {
        GLES20.glDeleteProgram(mProgram);
        mProgram = createProgram(VERTEX_SHADER, fragmentShader);

        Log.i("fragment_shader", fragmentShader+"");

        if (mProgram == 0) {
            throw new RuntimeException("failed creating program");
        }

        mvPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
        checkGlError("glGetAttribLocation vPosition");
        if (mvPositionHandle == -1) {
            throw new RuntimeException(
                    "Could not get attrib location for vPosition");
        }
        maTextureHandle = GLES20
                .glGetAttribLocation(mProgram, "vTexCoordinate");
        checkGlError("glGetAttribLocation vTexCoordinate");
        if (maTextureHandle == -1) {
            throw new RuntimeException(
                    "Could not get attrib location for vTexCoordinate");
        }

        muMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        checkGlError("glGetUniformLocation uMVPMatrix");
        if (muMVPMatrixHandle == -1) {
            throw new RuntimeException(
                    "Could not get attrib location for uMVPMatrix");
        }

        muSTMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uSTMatrix");
        Log.i("muSTMatrixHandle", "loc = "+muSTMatrixHandle);
        checkGlError("glGetUniformLocation uSTMatrix");
        if (muSTMatrixHandle == -1) {
            throw new RuntimeException(
                    "Could not get attrib location for uSTMatrix");
        }
    }

    private int loadShader(int shaderType, String source) {
        int shader = GLES20.glCreateShader(shaderType);
        checkGlError("glCreateShader type=" + shaderType);
        GLES20.glShaderSource(shader, source);
        GLES20.glCompileShader(shader);
        int[] compiled = new int[1];
        GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
        if (compiled[0] == 0) {
            Log.e(TAG, "Could not compile shader " + shaderType + ":");
            Log.e(TAG, " " + GLES20.glGetShaderInfoLog(shader));
            GLES20.glDeleteShader(shader);
            shader = 0;
        }
        return shader;
    }

    private int createProgram(String vertexSource, String fragmentSource) {
        //filterSourceTexture2 = loadColormap(BitmapFactory.decodeResource(context.getResources(), R.drawable.nashville_map));
        int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexSource);
        if (vertexShader == 0) {
            return 0;
        }
        int pixelShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentSource);
        if (pixelShader == 0) {
            return 0;
        }

        int program = GLES20.glCreateProgram();
        checkGlError("glCreateProgram");
        if (program == 0) {
            Log.e(TAG, "Could not create program");
        }
        GLES20.glAttachShader(program, vertexShader);
        checkGlError("glAttachShader");
        GLES20.glAttachShader(program, pixelShader);
        checkGlError("glAttachShader");
        GLES20.glLinkProgram(program);
        int[] linkStatus = new int[1];
        GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, linkStatus, 0);
        if (linkStatus[0] != GLES20.GL_TRUE) {
            Log.e(TAG, "Could not link program: ");
            Log.e(TAG, GLES20.glGetProgramInfoLog(program));
            GLES20.glDeleteProgram(program);
            program = 0;
        }

        //new program, new attribute locations!
        //update mvPositionHandle, maTextureHandle, mUVPMatrixHandle, and muSTMatrixHandle here

        return program;
    }

    public void checkGlError(String op) {
        int error;
        //Log.d(TAG, "error at "+op);
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, op + ": glError " + error);
            throw new RuntimeException(op + ": glError " + error);
        }
    }

    public void textureCleanup()
    {
        if (filterSourceTexture2 != -1) {
            int[] arrayOfInt1 = new int[1];
            arrayOfInt1[0] = this.filterSourceTexture2;
            GLES20.glDeleteTextures(1, arrayOfInt1, 0);
            this.filterSourceTexture2 = -1;
        }

        if (filterSourceTexture3 != -1) {
            int[] arrayOfInt2 = new int[1];
            arrayOfInt2[0] = this.filterSourceTexture3;
            GLES20.glDeleteTextures(1, arrayOfInt2, 0);
            this.filterSourceTexture3 = -1;
        }

        if (filterSourceTexture4 != -1) {
            int[] arrayOfInt3 = new int[1];
            arrayOfInt3[0] = this.filterSourceTexture4;
            GLES20.glDeleteTextures(1, arrayOfInt3, 0);
            this.filterSourceTexture4 = -1;
        }

        if (filterSourceTexture5 != -1) {
            int[] arrayOfInt4 = new int[1];
            arrayOfInt4[0] = this.filterSourceTexture5;
            GLES20.glDeleteTextures(1, arrayOfInt4, 0);
            this.filterSourceTexture5 = -1;
        }

        if (filterSourceTexture6 != -1) {
            int[] arrayOfInt5 = new int[1];
            arrayOfInt5[0] = this.filterSourceTexture6;
            GLES20.glDeleteTextures(1, arrayOfInt5, 0);
            this.filterSourceTexture6 = -1;
        }
    }
}
