#extension GL_OES_EGL_image_external : require

precision lowp float;
 
varying highp vec2 v_TexCoordinate;
 
uniform samplerExternalOES u_Texture;
uniform sampler2D inputImageTexture2;  //edgeBurn
uniform sampler2D inputImageTexture3;  //hefeMap
uniform sampler2D inputImageTexture4;  //hefeGradientMap
uniform sampler2D inputImageTexture5;  //hefeSoftLight
uniform sampler2D inputImageTexture6;  //hefeMetal

uniform float uParamValue1;

void main()
{
    vec3 texel = texture2D(u_Texture, v_TexCoordinate).rgb;
    vec3 procTexel;
    vec3 edge = texture2D(inputImageTexture2, v_TexCoordinate).rgb;
    procTexel = texel * edge;

    procTexel = vec3(
                     texture2D(inputImageTexture3, vec2(procTexel.r, .16666)).r,
                     texture2D(inputImageTexture3, vec2(procTexel.g, .5)).g,
                     texture2D(inputImageTexture3, vec2(procTexel.b, .83333)).b);

    vec3 luma = vec3(.30, .59, .11);
    vec3 gradSample = texture2D(inputImageTexture4, vec2(dot(luma, procTexel), .5)).rgb;
    vec3 final = vec3(
                      texture2D(inputImageTexture5, vec2(gradSample.r, procTexel.r)).r,
                      texture2D(inputImageTexture5, vec2(gradSample.g, procTexel.g)).g,
                      texture2D(inputImageTexture5, vec2(gradSample.b, procTexel.b)).b
                      );
    
    vec3 metal = texture2D(inputImageTexture6, v_TexCoordinate).rgb;
    vec3 metaled = vec3(
                        texture2D(inputImageTexture5, vec2(metal.r, procTexel.r)).r,
                        texture2D(inputImageTexture5, vec2(metal.g, procTexel.g)).g,
                        texture2D(inputImageTexture5, vec2(metal.b, procTexel.b)).b
                        );

    texel += (metaled - texel) * uParamValue1;

    gl_FragColor = vec4(texel, 1.0);
}