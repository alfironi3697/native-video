#extension GL_OES_EGL_image_external : require
precision lowp float;

varying highp vec2 v_TexCoordinate;

uniform samplerExternalOES u_Texture;
uniform sampler2D inputImageTexture2;
uniform sampler2D inputImageTexture3;
uniform sampler2D inputImageTexture4;

uniform float uParamValue1;

void main()
{
    vec4 texel = texture2D(u_Texture, v_TexCoordinate);
    vec3 bbTexel = texture2D(inputImageTexture2, v_TexCoordinate).rgb;
    vec3 tempTexel;
	
    tempTexel.r = texture2D(inputImageTexture3, vec2(bbTexel.r, texel.r)).r;
    tempTexel.g = texture2D(inputImageTexture3, vec2(bbTexel.g, texel.g)).g;
    tempTexel.b = texture2D(inputImageTexture3, vec2(bbTexel.b, texel.b)).b;
	
    vec4 mapped;
	mapped.r = texture2D(inputImageTexture4, vec2(tempTexel.r, .16666)).r;
    mapped.g = texture2D(inputImageTexture4, vec2(tempTexel.g, .5)).g;
    mapped.b = texture2D(inputImageTexture4, vec2(tempTexel.b, .83333)).b;
    mapped.a = 1.0;

    vec4 delta = texel - mapped;
    texel.rgb += delta.rgb * uParamValue1;
    texel.a = 1.0;
	
    gl_FragColor = texel;
}