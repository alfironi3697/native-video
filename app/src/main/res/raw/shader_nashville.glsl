#extension GL_OES_EGL_image_external : require
precision lowp float;

varying vec2 v_TexCoordinate;

uniform samplerExternalOES u_Texture;
uniform sampler2D inputImageTexture2;

uniform float uParamValue1;

void main()
{
    vec3 texel = texture2D(u_Texture, v_TexCoordinate).rgb;
    vec3 procTexel = vec3(
                 texture2D(inputImageTexture2, vec2(texel.r, .16666)).r,
                 texture2D(inputImageTexture2, vec2(texel.g, .5)).g,
                 texture2D(inputImageTexture2, vec2(texel.b, .83333)).b);
    texel += (procTexel - texel) * uParamValue1;
    gl_FragColor = vec4(texel, 1.0);
}