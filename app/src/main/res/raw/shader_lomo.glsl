#extension GL_OES_EGL_image_external : require

precision lowp float;

varying highp vec2 v_TexCoordinate;

uniform samplerExternalOES u_Texture;
uniform sampler2D inputImageTexture2;
uniform sampler2D inputImageTexture3;

uniform float uParamValue1;

void main()
{
    
    vec3 texel = texture2D(u_Texture, v_TexCoordinate).rgb;
    vec3 procTexel;
    
    vec2 red = vec2(texel.r, 0.16666);
    vec2 green = vec2(texel.g, 0.5);
    vec2 blue = vec2(texel.b, 0.83333);
    
    procTexel.rgb = vec3(
                     texture2D(inputImageTexture2, red).r,
                     texture2D(inputImageTexture2, green).g,
                     texture2D(inputImageTexture2, blue).b);
    
    vec2 tc = (2.0 * v_TexCoordinate) - 1.0;
    float d = dot(tc, tc);
    vec2 lookup = vec2(d, procTexel.r);
    procTexel.r = texture2D(inputImageTexture3, lookup).r;
    lookup.y = procTexel.g;
    procTexel.g = texture2D(inputImageTexture3, lookup).g;
    lookup.y = procTexel.b;
    procTexel.b = texture2D(inputImageTexture3, lookup).b;

    texel += (procTexel - texel) * uParamValue1;
    
    gl_FragColor = vec4(texel,1.0);
}