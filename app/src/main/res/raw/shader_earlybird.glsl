#extension GL_OES_EGL_image_external : require
precision lowp float;

varying highp vec2 v_TexCoordinate;

uniform samplerExternalOES u_Texture;
uniform sampler2D inputImageTexture2; //earlyBirdCurves
uniform sampler2D inputImageTexture3; //earlyBirdOverlay
uniform sampler2D inputImageTexture4; //vig
uniform sampler2D inputImageTexture5; //earlyBirdBlowout
uniform sampler2D inputImageTexture6; //earlyBirdMap

uniform float uParamValue1;

const mat3 saturate = mat3(
                           1.210300,
                           -0.089700,
                           -0.091000,
                           -0.176100,
                           1.123900,
                           -0.177400,
                           -0.034200,
                           -0.034200,
                           1.265800);
const vec3 rgbPrime = vec3(0.25098, 0.14640522, 0.0); 
const vec3 desaturate = vec3(.3, .59, .11);

void main()
{
    
    vec3 texel = texture2D(u_Texture, v_TexCoordinate).rgb;
    vec3 procTexel;
    
    vec2 lookup;    
    lookup.y = 0.5;
    
    lookup.x = texel.r;
    procTexel.r = texture2D(inputImageTexture2, lookup).r;
    
    lookup.x = texel.g;
    procTexel.g = texture2D(inputImageTexture2, lookup).g;
    
    lookup.x = texel.b;
    procTexel.b = texture2D(inputImageTexture2, lookup).b;
    
    float desaturatedColor;
    vec3 result;
    desaturatedColor = dot(desaturate, procTexel);
    
    
    lookup.x = desaturatedColor;
    result.r = texture2D(inputImageTexture3, lookup).r;
    lookup.x = desaturatedColor;
    result.g = texture2D(inputImageTexture3, lookup).g;
    lookup.x = desaturatedColor;
    result.b = texture2D(inputImageTexture3, lookup).b;
    
    procTexel = saturate * mix(procTexel, result, .5);
    
    vec2 tc = (2.0 * v_TexCoordinate) - 1.0;
    float d = dot(tc, tc);
    
    vec3 sampled;
    lookup.y = .5;
    
    //---
    
    lookup = vec2(d, procTexel.r);
    procTexel.r = texture2D(inputImageTexture4, lookup).r;
    lookup.y = procTexel.g;
    procTexel.g = texture2D(inputImageTexture4, lookup).g;
    lookup.y = procTexel.b;
    procTexel.b= texture2D(inputImageTexture4, lookup).b;
    float value = smoothstep(0.0, 1.25, pow(d, 1.35)/1.65);
    
    //---
    
    lookup.x = procTexel.r;
    sampled.r = texture2D(inputImageTexture5, lookup).r;
    lookup.x = procTexel.g;
    sampled.g = texture2D(inputImageTexture5, lookup).g;
    lookup.x = procTexel.b;
    sampled.b = texture2D(inputImageTexture5, lookup).b;
    procTexel = mix(sampled, procTexel, value);
    
    
    lookup.x = procTexel.r;
    procTexel.r = texture2D(inputImageTexture6, lookup).r;
    lookup.x = procTexel.g;
    procTexel.g = texture2D(inputImageTexture6, lookup).g;
    lookup.x = procTexel.b;
    procTexel.b = texture2D(inputImageTexture6, lookup).b;

    texel += (procTexel - texel) * uParamValue1;
    
    gl_FragColor = vec4(texel, 1.0);
}