#extension GL_OES_EGL_image_external : require

precision lowp float;

varying highp vec2 v_TexCoordinate;

uniform samplerExternalOES u_Texture;
uniform sampler2D inputImageTexture2; //map
uniform sampler2D inputImageTexture3; //vigMap

uniform float uParamValue1;

void main()
{
    
    vec3 texel = texture2D(u_Texture, v_TexCoordinate).rgb;
    vec3 procTexel;
    
    vec2 tc = (2.0 * v_TexCoordinate) - 1.0;
    float d = dot(tc, tc);
    vec2 lookup = vec2(d, texel.r);
    procTexel.r = texture2D(inputImageTexture3, lookup).r;
    lookup.y = texel.g;
    procTexel.g = texture2D(inputImageTexture3, lookup).g;
    lookup.y = texel.b;
    procTexel.b = texture2D(inputImageTexture3, lookup).b;
    
    vec2 red = vec2(procTexel.r, 0.16666);
    vec2 green = vec2(procTexel.g, 0.5);
    vec2 blue = vec2(procTexel.b, .83333);
    procTexel.r = texture2D(inputImageTexture2, red).r;
    procTexel.g = texture2D(inputImageTexture2, green).g;
    procTexel.b = texture2D(inputImageTexture2, blue).b;

    texel += (procTexel - texel)*uParamValue1;
    
    gl_FragColor = vec4(texel, 1.0);
    
}