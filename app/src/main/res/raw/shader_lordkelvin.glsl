#extension GL_OES_EGL_image_external : require

precision lowp float;

varying highp vec2 v_TexCoordinate;

uniform samplerExternalOES u_Texture;
uniform sampler2D inputImageTexture2;

uniform float uParamValue1;

void main()
{
    vec3 texel = texture2D(u_Texture, v_TexCoordinate).rgb;

    vec2 lookup;
    vec3 texelCompare;
    lookup.y = .5;

    lookup.x = texel.r;
    texelCompare.r = texture2D(inputImageTexture2, lookup).r;

    lookup.x = texel.g;
    texelCompare.g = texture2D(inputImageTexture2, lookup).g;

    lookup.x = texel.b;
    texelCompare.b = texture2D(inputImageTexture2, lookup).b;

    vec3 deltaTexel = texelCompare - texel;
    texel += deltaTexel * uParamValue1;

    gl_FragColor = vec4(texel, 1.0);
}