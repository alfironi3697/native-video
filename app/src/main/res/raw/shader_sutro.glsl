#extension GL_OES_EGL_image_external : require

precision lowp float;

varying highp vec2 v_TexCoordinate;

uniform samplerExternalOES u_Texture;
uniform sampler2D inputImageTexture2; //sutroMap;
uniform sampler2D inputImageTexture3; //sutroMetal;
uniform sampler2D inputImageTexture4; //softLight
uniform sampler2D inputImageTexture5; //sutroEdgeburn
uniform sampler2D inputImageTexture6; //sutroCurves

uniform float uParamValue1;

void main()
{
    
    vec3 texel = texture2D(u_Texture, v_TexCoordinate).rgb;
    vec3 procTexel;

    vec2 tc = (2.0 * v_TexCoordinate) - 1.0;
    float d = dot(tc, tc);
    vec2 lookup = vec2(d, texel.r);
    procTexel.r = texture2D(inputImageTexture2, lookup).r;
    lookup.y = texel.g;
    procTexel.g = texture2D(inputImageTexture2, lookup).g;
    lookup.y = texel.b;
    procTexel.b = texture2D(inputImageTexture2, lookup).b;
    
    vec3 rgbPrime = vec3(0.1019, 0.0, 0.0); 
    float m = dot(vec3(.3, .59, .11), procTexel.rgb) - 0.03058;
    procTexel = mix(procTexel, rgbPrime + m, 0.32);
    
    vec3 metal = texture2D(inputImageTexture3, v_TexCoordinate).rgb;
    procTexel.r = texture2D(inputImageTexture4, vec2(metal.r, procTexel.r)).r;
    procTexel.g = texture2D(inputImageTexture4, vec2(metal.g, procTexel.g)).g;
    procTexel.b = texture2D(inputImageTexture4, vec2(metal.b, procTexel.b)).b;
    
    procTexel = procTexel * texture2D(inputImageTexture5, v_TexCoordinate).rgb;
    
    procTexel.r = texture2D(inputImageTexture6, vec2(procTexel.r, .16666)).r;
    procTexel.g = texture2D(inputImageTexture6, vec2(procTexel.g, .5)).g;
    procTexel.b = texture2D(inputImageTexture6, vec2(procTexel.b, .83333)).b;

    texel += (procTexel - texel) * uParamValue1;
    
    gl_FragColor = vec4(texel, 1.0);
}