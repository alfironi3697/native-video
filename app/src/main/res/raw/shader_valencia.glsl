#extension GL_OES_EGL_image_external : require

precision lowp float;

varying highp vec2 v_TexCoordinate;

uniform samplerExternalOES u_Texture;
uniform sampler2D inputImageTexture2; //map
uniform sampler2D inputImageTexture3; //gradMap

uniform float uParamValue1;

mat3 saturateMatrix = mat3(
                           1.1402,
                           -0.0598,
                           -0.061,
                           -0.1174,
                           1.0826,
                           -0.1186,
                           -0.0228,
                           -0.0228,
                           1.1772);

vec3 lumaCoeffs = vec3(.3, .59, .11);

void main()
{
    vec3 texel = texture2D(u_Texture, v_TexCoordinate).rgb;
    
    vec3 procTexel = vec3(
                 texture2D(inputImageTexture2, vec2(texel.r, .1666666)).r,
                 texture2D(inputImageTexture2, vec2(texel.g, .5)).g,
                 texture2D(inputImageTexture2, vec2(texel.b, .8333333)).b
                 );
    
    procTexel = saturateMatrix * procTexel;
    float luma = dot(lumaCoeffs, procTexel);
    procTexel = vec3(
                 texture2D(inputImageTexture3, vec2(luma, procTexel.r)).r,
                 texture2D(inputImageTexture3, vec2(luma, procTexel.g)).g,
                 texture2D(inputImageTexture3, vec2(luma, procTexel.b)).b);

    texel += (procTexel - texel) * uParamValue1;
    
    gl_FragColor = vec4(texel, 1.0);
}