#extension GL_OES_EGL_image_external : require
precision mediump float;

uniform samplerExternalOES u_Texture;
uniform sampler2D inputImageTexture2;

varying highp vec2 v_TexCoordinate;

uniform float uParamValue1; //filter strength

void main()
{
    vec3 texel = texture2D(u_Texture, v_TexCoordinate).rgb;
    vec3 newTexel = vec3(
        texture2D(inputImageTexture2, vec2(texel.r, .16666)).r,
        texture2D(inputImageTexture2, vec2(texel.g, .5)).g,
        texture2D(inputImageTexture2, vec2(texel.b, .83333)).b
    );

    texel += (newTexel - texel) * uParamValue1;

    gl_FragColor = vec4(texel, 1.0);
}