#extension GL_OES_EGL_image_external : require
precision mediump float;

varying highp vec2 v_TexCoordinate;

uniform samplerExternalOES u_Texture;
uniform sampler2D inputImageTexture2;

const float uParamValue1 = 1.0;

void main()
{
    vec3 texel = texture2D(u_Texture, v_TexCoordinate).rgb;
    vec3 procTexel = vec3(dot(vec3(0.3, 0.6, 0.1), texel));

    procTexel = vec3(texture2D(inputImageTexture2, vec2(procTexel.r, .16666)).r);
    texel += (procTexel - texel) * uParamValue1;

    gl_FragColor = vec4(texel, 1.0);
}