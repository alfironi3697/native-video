#extension GL_OES_EGL_image_external : require

precision lowp float;

varying highp vec2 v_TexCoordinate;

uniform samplerExternalOES u_Texture;
uniform sampler2D inputImageTexture2;  //process
uniform sampler2D inputImageTexture3;  //blowout
uniform sampler2D inputImageTexture4;  //contrast
uniform sampler2D inputImageTexture5;  //luma
uniform sampler2D inputImageTexture6;  //screen

uniform float uParamValue1; // filter strength, as always

mat3 saturateMatrix = mat3(
                           1.105150,
                           -0.044850,
                           -0.046000,
                           -0.088050,
                           1.061950,
                           -0.089200,
                           -0.017100,
                           -0.017100,
                           1.132900);

vec3 luma = vec3(.3, .59, .11);

void main()
{
    
    vec3 texel = texture2D(u_Texture, v_TexCoordinate).rgb;
    vec3 procTexel;
    
    vec2 lookup;
    lookup.y = 0.5;
    lookup.x = texel.r;
    procTexel.r = texture2D(inputImageTexture2, lookup).r;
    lookup.x = texel.g;
    procTexel.g = texture2D(inputImageTexture2, lookup).g;
    lookup.x = texel.b;
    procTexel.b = texture2D(inputImageTexture2, lookup).b;
    
    procTexel = saturateMatrix * procTexel;
    
    
    vec2 tc = (2.0 * v_TexCoordinate) - 1.0;
    float d = dot(tc, tc);
    vec3 sampled;
    lookup.y = 0.5;
    lookup.x = procTexel.r;
    sampled.r = texture2D(inputImageTexture3, lookup).r;
    lookup.x = procTexel.g;
    sampled.g = texture2D(inputImageTexture3, lookup).g;
    lookup.x = procTexel.b;
    sampled.b = texture2D(inputImageTexture3, lookup).b;
    float value = smoothstep(0.0, 1.0, d);
    procTexel = mix(sampled, procTexel, value);
    
    lookup.x = procTexel.r;
    procTexel.r = texture2D(inputImageTexture4, lookup).r;
    lookup.x = procTexel.g;
    procTexel.g = texture2D(inputImageTexture4, lookup).g;
    lookup.x = procTexel.b;
    procTexel.b = texture2D(inputImageTexture4, lookup).b;
    
    
    lookup.x = dot(procTexel, luma);
    procTexel = mix(texture2D(inputImageTexture5, lookup).rgb, procTexel, .5);

    lookup.x = procTexel.r;
    procTexel.r = texture2D(inputImageTexture6, lookup).r;
    lookup.x = procTexel.g;
    procTexel.g = texture2D(inputImageTexture6, lookup).g;
    lookup.x = procTexel.b;
    procTexel.b = texture2D(inputImageTexture6, lookup).b;

    texel += (procTexel - texel) * uParamValue1;
    
    gl_FragColor = vec4(texel, 1.0);
}